package eightam.memorygame.util;

public class ThreadUtils {

    public static void sleep(long durationMillis) {
        try {
            Thread.sleep(durationMillis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
