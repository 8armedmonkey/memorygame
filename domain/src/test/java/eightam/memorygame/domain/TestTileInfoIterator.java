package eightam.memorygame.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class TestTileInfoIterator {

    private TileInfoIterator iterator;

    @Before
    public void setUp() {
        List<Tile> tiles = new ArrayList<>();
        tiles.add(new TileImpl(0, Tile.Status.MATCH));
        tiles.add(new TileImpl(1, Tile.Status.OPEN));
        tiles.add(new TileImpl(1, Tile.Status.CLOSE));
        tiles.add(new TileImpl(0, Tile.Status.MATCH));

        iterator = new TileInfoIterator(tiles);
    }

    @Test
    public void hasNext_nextExists_returnTrue() {
        assertTrue(iterator.hasNext());
    }

    @Test
    public void hasNext_nextNotExists_returnFalse() {
        iterator.next();
        iterator.next();
        iterator.next();
        iterator.next();

        assertFalse(iterator.hasNext());
    }

    @Test
    public void next_returnTileInfo() {
        TileInfo tf;

        tf = iterator.next();
        assertEquals(0, tf.getNumericValue());
        assertEquals(Tile.Status.MATCH, tf.getStatus());

        tf = iterator.next();
        assertEquals(1, tf.getNumericValue());
        assertEquals(Tile.Status.OPEN, tf.getStatus());

        tf = iterator.next();
        assertEquals(1, tf.getNumericValue());
        assertEquals(Tile.Status.CLOSE, tf.getStatus());

        tf = iterator.next();
        assertEquals(0, tf.getNumericValue());
        assertEquals(Tile.Status.MATCH, tf.getStatus());
    }

}
