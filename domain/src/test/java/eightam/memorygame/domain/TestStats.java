package eightam.memorygame.domain;

import org.junit.Before;
import org.junit.Test;

import static eightam.memorygame.domain.Factories.newStatsBuilder;
import static org.junit.Assert.*;

public class TestStats {

    private Stats.Builder builder;

    @Before
    public void setUp() {
        builder = newStatsBuilder();
    }

    @Test
    public void setNumberOfTiles_numberOfTilesIsSet() {
        Stats stats = builder.numberOfTiles(16).build();
        assertEquals(16, stats.getNumberOfTiles());
    }

    @Test
    public void setNumberOfMatchedTiles_numberOfMatchedTilesIsSet() {
        Stats stats = builder.numberOfTiles(8).numberOfMatchedTiles(8).numberOfAttempts(4).build();
        assertEquals(8, stats.getNumberOfMatchedTiles());
    }

    @Test
    public void isCompleted_numberOfTilesIsEqualToNumberOfMatchedTiles_returnTrue() {
        Stats stats = builder.numberOfTiles(8).numberOfMatchedTiles(8).numberOfAttempts(4).build();
        assertTrue(stats.isCompleted());
    }

    @Test
    public void isCompleted_numberOfTilesIsNotEqualToNumberOfMatchedTiles_returnFalse() {
        Stats stats = builder.numberOfTiles(8).numberOfMatchedTiles(4).numberOfAttempts(2).build();
        assertFalse(stats.isCompleted());
    }

    @Test
    public void getCompletenessPercentage_returnCompletenessPercentage() {
        Stats stats = builder
                .numberOfTiles(8)
                .numberOfMatchedTiles(2)
                .numberOfAttempts(5)
                .durationMillis(5000)
                .durationToCompleteMillis(1000)
                .build();

        assertEquals(0.25, stats.getCompletenessPercentage(), 0.01);
    }

    @Test
    public void setNumberOfAttempts_numberOfAttemptsIsSet() {
        Stats stats = builder.numberOfAttempts(5).build();
        assertEquals(5, stats.getNumberOfAttempts());
    }

    @Test
    public void getAttemptsPercentage_returnAttemptsPercentage() {
        Stats stats = builder
                .numberOfTiles(8)
                .numberOfMatchedTiles(2)
                .numberOfAttempts(5)
                .durationMillis(5000)
                .durationToCompleteMillis(1000)
                .build();

        assertEquals(0.8, stats.getAttemptsPercentage(), 0.01);
    }

    @Test
    public void setDurationMillis_durationMillisIsSet() {
        Stats stats = builder.durationMillis(5000).build();
        assertEquals(5000, stats.getDurationMillis());
    }

    @Test
    public void setDurationToCompleteMillis_durationToCompleteMillisIsSet() {
        Stats stats = builder.durationMillis(5000).durationToCompleteMillis(3000).build();
        assertEquals(3000, stats.getDurationToCompleteMillis());
    }

    @Test
    public void getDurationPercentage_returnDurationPercentage() {
        Stats stats = builder
                .numberOfTiles(8)
                .numberOfMatchedTiles(2)
                .numberOfAttempts(5)
                .durationMillis(5000)
                .durationToCompleteMillis(2000)
                .build();

        assertEquals(0.6, stats.getDurationPercentage(), 0.01);
    }

    @Test(expected = InvalidStatsException.class)
    public void build_numberOfTilesIsLessThanNumberOfMatchedTiles() {
        builder.numberOfTiles(4).numberOfMatchedTiles(8).numberOfAttempts(4).build();
    }

    @Test(expected = InvalidStatsException.class)
    public void build_numberOfAttemptsIsLessThanHalfNumberOfMatchedTiles() {
        builder.numberOfTiles(16).numberOfMatchedTiles(8).numberOfAttempts(1).build();
    }

    @Test(expected = InvalidStatsException.class)
    public void build_durationMillisIsLessThanDurationToCompleteMillis() {
        builder.durationMillis(1000).durationToCompleteMillis(2000).build();
    }

}
