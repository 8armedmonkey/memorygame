package eightam.memorygame.domain;

import org.junit.Test;
import org.mockito.ArgumentCaptor;
import sys.event.Event;
import sys.event.EventBroadcaster;

import java.lang.reflect.InvocationTargetException;
import java.util.List;

import static eightam.memorygame.domain.Factories.newBoardSize;
import static eightam.memorygame.domain.Factories.newGameSpecsBuilder;
import static eightam.memorygame.util.AssertionUtils.assertUtilityClassWellDefined;
import static eightam.memorygame.util.ThreadUtils.sleep;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.*;

public class TestFactories {

    @Test
    public void wellDefinedUtilityClass() {
        try {
            assertUtilityClassWellDefined(Factories.class);

        } catch (NoSuchMethodException
                | InvocationTargetException
                | InstantiationException
                | IllegalAccessException e) {
            fail();
        }
    }

    @Test
    public void newGame_gameSpecs_boardInitialized() {
        GameSpecs specs = newGameSpecsBuilder()
                .boardSize(newBoardSize(4))
                .durationMillis(3000)
                .build();

        Game game = Factories.newGame(
                specs, mock(StatsTracker.class), mock(EventBroadcaster.class));

        game.start();

        // Opening tile index 0 will not throw exception because the board has been initialized.
        game.open(0);
    }

    @Test
    public void newGame_gameSpecs_timerInitialized() {
        GameSpecs specs = newGameSpecsBuilder()
                .boardSize(newBoardSize(4))
                .durationMillis(3000)
                .build();

        EventBroadcaster eventBroadcaster = mock(EventBroadcaster.class);

        Game game = Factories.newGame(
                specs, mock(StatsTracker.class), eventBroadcaster);

        game.start();

        sleep(3100);

        // Since we don't specify the tick interval then the default should be used.
        ArgumentCaptor<Event> captor = ArgumentCaptor.forClass(Event.class);
        verify(eventBroadcaster, atLeastOnce()).broadcast(captor.capture());

        List<Event> values = captor.getAllValues();

        assertEquals(GameStartedEvent.class, values.get(0).getClass());

        for (int i = 1; i < values.size() - 1; i++) {
            if (!GameTimerTickEvent.class.equals(values.get(i).getClass())) {
                fail();
            }
        }

        assertEquals(GameTimerExpiredEvent.class, values.get(values.size() - 1).getClass());
    }

}
