package eightam.memorygame.domain;

import org.junit.Before;
import org.junit.Test;

import static eightam.memorygame.domain.CountdownTimerImpl.DEFAULT_TICK_INTERVAL_MILLIS;
import static eightam.memorygame.domain.Factories.newCountdownTimer;
import static eightam.memorygame.util.ThreadUtils.sleep;
import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

public class TestCountdownTimer {

    private CountdownTimer timer;

    @Before
    public void setUp() {
        timer = newCountdownTimer(100, 2000);
    }

    @Test
    public void constructor_noTickIntervalMillisSpecified_defaultTickIntervalMillisIsUsed() {
        timer = newCountdownTimer(2000);

        CountdownTimer.Listener listener = mock(CountdownTimer.Listener.class);
        timer.setListener(listener);

        timer.start();

        sleep(550);

        verify(listener, times((int) (550 / DEFAULT_TICK_INTERVAL_MILLIS) + 1)).onTimerTick();
    }

    @Test
    public void start_timerStarted() {
        CountdownTimer.Listener listener = mock(CountdownTimer.Listener.class);
        timer.setListener(listener);
        timer.start();

        sleep(550);

        assertTrue(timer.isRunning());

        timer.pause();

        // 0, 100, 200, 300, 400, 500.
        verify(listener, times(6)).onTimerTick();
    }

    @Test
    public void pause_timerPaused() {
        CountdownTimer.Listener listener = mock(CountdownTimer.Listener.class);
        timer.setListener(listener);
        timer.start();

        sleep(350);

        timer.pause();

        assertFalse(timer.isRunning());

        sleep(250);

        // 0, 100, 200, 300.
        verify(listener, times(4)).onTimerTick();
    }

    @Test
    public void timerExpired_listenerNotified() {
        CountdownTimer.Listener listener = mock(CountdownTimer.Listener.class);
        timer.setListener(listener);
        timer.start();

        sleep(2050);

        verify(listener).onTimerExpired();
    }

    @Test
    public void timerExpired_isRunningReturnFalse() {
        CountdownTimer.Listener listener = mock(CountdownTimer.Listener.class);
        timer.setListener(listener);
        timer.start();

        sleep(2050);

        assertFalse(timer.isRunning());
    }

    @Test
    public void timerExpired_isExpiredReturnTrue() {
        CountdownTimer.Listener listener = mock(CountdownTimer.Listener.class);
        timer.setListener(listener);
        timer.start();

        sleep(2050);

        assertTrue(timer.isExpired());
    }

    @Test
    public void timerNotExpired_isExpiredReturnFalse() {
        CountdownTimer.Listener listener = mock(CountdownTimer.Listener.class);
        timer.setListener(listener);
        timer.start();

        sleep(500);

        assertFalse(timer.isExpired());
    }

    @Test
    public void reset_timerPausedAndElapsedTimeReset() {
        CountdownTimer.Listener listener = mock(CountdownTimer.Listener.class);
        timer.setListener(listener);
        timer.start();

        sleep(550);

        timer.reset();

        sleep(350);

        // 0, 100, 200, 300, 400, 500.
        verify(listener, times(6)).onTimerTick();
        verify(listener).onTimerReset();

        assertFalse(timer.isRunning());
        assertEquals(0, timer.getElapsedMillis());
    }

    @Test
    public void getDurationMillis_returnDurationMillis() {
        assertEquals(2000, timer.getDurationMillis());
    }

}
