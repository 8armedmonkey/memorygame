package eightam.memorygame.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestTileInfo {

    private TileInfo tileInfo;

    @Before
    public void setUp() {
        tileInfo = new TileInfoImpl(new TileImpl(1, Tile.Status.OPEN));
    }

    @Test
    public void getNumericValue_tileNumericValueIsReturned() {
        assertEquals(1, tileInfo.getNumericValue());
    }

    @Test
    public void getStatus_tileStatusIsReturned() {
        assertEquals(Tile.Status.OPEN, tileInfo.getStatus());
    }

}
