package eightam.memorygame.domain;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestStatsTracker {

    private StatsTracker statsTracker;

    @Before
    public void setUp() {
        statsTracker = Factories.newStatsTracker();
    }

    @Test
    public void incrementNumberOfAttempts_numberOfAttemptsIncrementedBy1() {
        statsTracker.incrementNumberOfAttempts();
        assertEquals(1, statsTracker.getNumberOfAttempts());
    }

    @Test
    public void incrementNumberOfMatchedTiles_numberOfMatchedTilesIncrementedBy2() {
        statsTracker.incrementNumberOfMatchedTiles();
        assertEquals(2, statsTracker.getNumberOfMatchedTiles());
    }

    @Test
    public void reset_statsAreReset() {
        statsTracker.incrementNumberOfAttempts();
        statsTracker.incrementNumberOfMatchedTiles();
        statsTracker.reset();

        assertEquals(0, statsTracker.getNumberOfAttempts());
        assertEquals(0, statsTracker.getNumberOfMatchedTiles());
    }

}
