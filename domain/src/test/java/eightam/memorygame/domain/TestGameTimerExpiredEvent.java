package eightam.memorygame.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class TestGameTimerExpiredEvent {

    @Test
    public void constructor_statsIsSet() {
        Stats stats = mock(Stats.class);
        assertEquals(stats, new GameTimerExpiredEvent(stats).getStats());
    }

}
