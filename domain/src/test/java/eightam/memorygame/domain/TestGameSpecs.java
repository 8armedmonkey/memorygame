package eightam.memorygame.domain;

import org.junit.Before;
import org.junit.Test;

import static eightam.memorygame.domain.Factories.newBoardSize;
import static eightam.memorygame.domain.Factories.newGameSpecsBuilder;
import static org.junit.Assert.assertEquals;

public class TestGameSpecs {

    private GameSpecs.Builder builder;

    @Before
    public void setUp() {
        builder = newGameSpecsBuilder();
    }

    @Test
    public void setBoardSize_boardSizeIsSet() {
        BoardSize boardSize = newBoardSize(4);
        assertEquals(boardSize, builder.boardSize(boardSize).build().getBoardSize());
    }

    @Test
    public void setDurationMillis_durationMillisIsSet() {
        long durationMillis = 1000;
        assertEquals(durationMillis, builder.durationMillis(durationMillis).build().getDurationMillis());
    }

}
