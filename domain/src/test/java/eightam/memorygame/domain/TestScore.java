package eightam.memorygame.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestScore {

    @Test
    public void constructor_valueIsSet() {
        assertEquals(99.9, new ScoreImpl(99.9).getValue(), 0.1);
    }

}
