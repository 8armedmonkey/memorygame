package eightam.memorygame.domain;

import org.junit.Before;
import org.junit.Test;

import static eightam.memorygame.domain.Factories.newScoringService;
import static eightam.memorygame.domain.Factories.newStatsBuilder;
import static org.junit.Assert.assertTrue;

public class TestScoringService {

    private ScoringService scoringService;

    @Before
    public void setUp() {
        scoringService = newScoringService();
    }

    @Test
    public void calculateCompleteness___moreCompleteBetterScore() {
        Score score1 = scoringService.calculateCompleteness(0.50);
        Score score2 = scoringService.calculateCompleteness(0.75);

        assertTrue(score1.getValue() < score2.getValue());
    }

    @Test
    public void calculate___betterStatsProducesBetterScore() {
        Stats.Builder builder = newStatsBuilder();

        Stats poorStats = builder
                .numberOfTiles(16)
                .numberOfMatchedTiles(4)
                .numberOfAttempts(100)
                .durationMillis(5000)
                .durationToCompleteMillis(5000)
                .build();

        Stats goodStats = builder
                .numberOfTiles(16)
                .numberOfMatchedTiles(12)
                .numberOfAttempts(6)
                .durationMillis(5000)
                .durationToCompleteMillis(1000)
                .build();

        Score score1 = scoringService.calculate(poorStats);
        Score score2 = scoringService.calculate(goodStats);

        assertTrue(score1.getValue() < score2.getValue());
    }

    @Test
    public void calculate___betterTilesPercentageProducesBetterScore() {
        Stats.Builder builder = newStatsBuilder();

        Stats poorStats = builder
                .numberOfTiles(16)
                .numberOfMatchedTiles(4)
                .numberOfAttempts(6)
                .durationMillis(5000)
                .durationToCompleteMillis(5000)
                .build();

        Stats goodStats = builder
                .numberOfTiles(16)
                .numberOfMatchedTiles(12)
                .numberOfAttempts(6)
                .durationMillis(5000)
                .durationToCompleteMillis(5000)
                .build();

        Score score1 = scoringService.calculate(poorStats);
        Score score2 = scoringService.calculate(goodStats);

        assertTrue(score1.getValue() < score2.getValue());
    }

    @Test
    public void calculate___betterAttemptsPercentageProducesBetterScore() {
        Stats.Builder builder = newStatsBuilder();

        Stats poorStats = builder
                .numberOfTiles(16)
                .numberOfMatchedTiles(16)
                .numberOfAttempts(100)
                .durationMillis(5000)
                .durationToCompleteMillis(5000)
                .build();

        Stats goodStats = builder
                .numberOfTiles(16)
                .numberOfMatchedTiles(16)
                .numberOfAttempts(8)
                .durationMillis(5000)
                .durationToCompleteMillis(5000)
                .build();

        Score score1 = scoringService.calculate(poorStats);
        Score score2 = scoringService.calculate(goodStats);

        assertTrue(score1.getValue() < score2.getValue());
    }

    @Test
    public void calculate___betterDurationPercentageProducesBetterScore() {
        Stats.Builder builder = newStatsBuilder();

        Stats poorStats = builder
                .numberOfTiles(16)
                .numberOfMatchedTiles(16)
                .numberOfAttempts(8)
                .durationMillis(5000)
                .durationToCompleteMillis(5000)
                .build();

        Stats goodStats = builder
                .numberOfTiles(16)
                .numberOfMatchedTiles(16)
                .numberOfAttempts(8)
                .durationMillis(5000)
                .durationToCompleteMillis(1000)
                .build();

        Score score1 = scoringService.calculate(poorStats);
        Score score2 = scoringService.calculate(goodStats);

        assertTrue(score1.getValue() < score2.getValue());
    }

}
