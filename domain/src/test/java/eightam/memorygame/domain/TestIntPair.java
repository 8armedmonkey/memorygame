package eightam.memorygame.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

public class TestIntPair {

    private IntPair pair;

    @Before
    public void setUp() {
        pair = new IntPairImpl();
    }

    @Test
    public void put_setFirst_firstNumberSet() {
        pair.put(2);
        assertEquals(2, pair.getFirst());
    }

    @Test(expected = ValueNotYetSetException.class)
    public void getFirst_firstNotSet_throwValueNotYetSetException() {
        pair.getFirst();
    }

    @Test(expected = ValueNotYetSetException.class)
    public void getSecond_secondNotSet_throwValueNotYetSetException() {
        pair.getSecond();
    }

    @Test
    public void put_setSecond_secondNumberSet() {
        pair.put(2);
        pair.put(4);
        assertEquals(4, pair.getSecond());
    }

    @Test(expected = MaxCapacityReachedException.class)
    public void put_setMoreThanTwoNumbers_throwMaxCapacityReachedException() {
        pair.put(4);
        pair.put(8);
        pair.put(16);
    }

    @Test
    public void removeFirst_firstNumberRemoved() {
        pair.put(2);
        pair.removeFirst();
        assertTrue(pair.isEmpty());
        assertFalse(pair.isFirstSet());
    }

    @Test
    public void removeFirst_secondNumberSet_secondNumberBecomeFirst() {
        pair.put(4);
        pair.put(8);
        pair.removeFirst();

        assertFalse(pair.isEmpty());
        assertTrue(pair.isFirstSet());
        assertFalse(pair.isSecondSet());
        assertEquals(8, pair.getFirst());
    }

    @Test
    public void removeSecond_secondNumberRemoved() {
        pair.put(4);
        pair.put(8);
        pair.removeSecond();

        assertFalse(pair.isSecondSet());
    }

    @Test(expected = ValueNotYetSetException.class)
    public void removeFirst_firstNumberNotSet_throwValueNotYetSetException() {
        pair.removeFirst();
    }

    @Test(expected = ValueNotYetSetException.class)
    public void removeSecond_secondNumberNotSet_throwValueNotYetSetException() {
        pair.put(2);
        pair.removeSecond();
    }

    @Test
    public void clear_firstAndSecondNumbersAreRemoved() {
        pair.put(2);
        pair.put(8);
        pair.clear();

        assertTrue(pair.isEmpty());
        assertFalse(pair.isFirstSet());
        assertFalse(pair.isSecondSet());
    }

    @Test
    public void iterator_returnIntIterator() {
        pair.put(5);
        pair.put(8);

        List<Integer> numbers = new ArrayList<>();

        for (int number : pair) {
            numbers.add(number);
        }

        assertEquals(5, numbers.get(0).intValue());
        assertEquals(8, numbers.get(1).intValue());
    }

}
