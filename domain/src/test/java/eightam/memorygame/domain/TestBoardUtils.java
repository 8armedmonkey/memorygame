package eightam.memorygame.domain;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

import static eightam.memorygame.util.AssertionUtils.assertUtilityClassWellDefined;
import static org.junit.Assert.fail;

public class TestBoardUtils {

    @Test
    public void wellDefinedUtilityClass() {
        try {
            assertUtilityClassWellDefined(BoardUtils.class);

        } catch (NoSuchMethodException
                | InvocationTargetException
                | InstantiationException
                | IllegalAccessException e) {
            fail();
        }
    }

}
