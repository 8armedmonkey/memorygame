package eightam.memorygame.domain;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

import static eightam.memorygame.util.AssertionUtils.assertUtilityClassWellDefined;
import static org.junit.Assert.fail;

public class TestTileUtils {

    @Test
    public void wellDefinedUtilityClass() {
        try {
            assertUtilityClassWellDefined(TileUtils.class);

        } catch (NoSuchMethodException
                | InvocationTargetException
                | InstantiationException
                | IllegalAccessException e) {
            fail();
        }
    }

}
