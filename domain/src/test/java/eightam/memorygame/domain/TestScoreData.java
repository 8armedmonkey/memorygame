package eightam.memorygame.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static eightam.memorygame.domain.Factories.newScoreDataBuilder;
import static org.junit.Assert.*;

public class TestScoreData {

    private ScoreData.Builder builder;

    @Before
    public void setUp() {
        builder = newScoreDataBuilder();
    }

    @Test
    public void builderPut_keyValuePairIsSet() {
        ScoreData scoreData = builder
                .put(new StringKeyValuePair("KEY_1", "VALUE_1"))
                .put(new StringKeyValuePair("KEY_2", "VALUE_2"))
                .build();

        assertTrue(scoreData.contains("KEY_1"));
        assertTrue(scoreData.contains("KEY_2"));
    }

    @Test
    public void put_keyValuePairIsSet() {
        ScoreData scoreData = builder.build();
        scoreData.put(new StringKeyValuePair("KEY_1", "VALUE_1"));

        assertTrue(scoreData.contains("KEY_1"));
    }

    @Test
    public void remove_keyValuePairIsRemoved() {
        ScoreData scoreData = builder
                .put(new StringKeyValuePair("KEY_1", "VALUE_1"))
                .put(new StringKeyValuePair("KEY_2", "VALUE_2"))
                .build();

        scoreData.remove("KEY_1");

        assertFalse(scoreData.contains("KEY_1"));
    }

    @Test
    public void size_sizeIsReturned() {
        ScoreData scoreData = builder
                .put(new StringKeyValuePair("KEY_1", "VALUE_1"))
                .put(new StringKeyValuePair("KEY_2", "VALUE_2"))
                .build();

        assertEquals(2, scoreData.size());
    }

    @Test
    public void iterator_returnScoreDataIterator() {
        StringKeyValuePair pair1 = new StringKeyValuePair("KEY_1", "VALUE_1");
        StringKeyValuePair pair2 = new StringKeyValuePair("KEY_2", "VALUE_2");

        Set<StringKeyValuePair> set1 = new HashSet<>();
        set1.add(pair1);
        set1.add(pair2);

        Set<StringKeyValuePair> set2 = new HashSet<>();

        ScoreData scoreData = builder
                .put(pair1)
                .put(pair2)
                .build();

        for (StringKeyValuePair pair : scoreData) {
            set2.add(pair);
        }

        assertEquals(set1, set2);
    }

}
