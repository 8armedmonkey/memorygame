package eightam.memorygame.domain;

import org.junit.Before;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import sys.event.Event;
import sys.event.EventBroadcaster;

import static eightam.memorygame.domain.Factories.newBoardSize;
import static eightam.memorygame.domain.Factories.newGame;
import static org.junit.Assert.*;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.*;

public class TestGame {

    private Board board;
    private CountdownTimer timer;
    private StatsTracker statsTracker;
    private EventBroadcaster eventBroadcaster;
    private Game game;

    @Before
    public void setUp() {
        board = mock(Board.class);
        timer = mock(CountdownTimer.class);
        statsTracker = mock(StatsTracker.class);
        eventBroadcaster = mock(EventBroadcaster.class);
        game = spy(newGame(board, timer, statsTracker, eventBroadcaster));
    }

    @Test
    public void init___gameStateIsSetToInit() {
        assertEquals(Game.State.INIT, game.getState());
    }

    @Test
    public void start___gameIsNotStartedYet___gameStateChangedToStarted() {
        game.start();
        assertEquals(Game.State.STARTED, game.getState());
    }

    @Test
    public void start___gameIsNotStartedYet___gameStartedEventBroadcast() {
        game.start();
        verify(eventBroadcaster).broadcast(any(GameStartedEvent.class));
    }

    @Test
    public void start___gameIsNotStartedYet___countdownTimerIsStarted() {
        game.start();
        verify(timer).start();
    }

    @Test
    public void start___gameIsAlreadyStarted___doNothing() {
        game.start();
        game.start();
        game.start();

        verify(eventBroadcaster, times(1)).broadcast(any(GameStartedEvent.class));
        verify(timer, times(1)).start();
    }

    @Test(expected = InvalidGameStateException.class)
    public void start___gameIsAlreadyFinished_boardComplete___throwInvalidGameStateException() {
        when(game.getState()).thenReturn(Game.State.COMPLETED);
        game.start();
    }

    @Test(expected = InvalidGameStateException.class)
    public void start___gameIsAlreadyFinished_timerExpired___throwInvalidGameStateException() {
        when(game.getState()).thenReturn(Game.State.TIMER_EXPIRED);
        game.start();
    }

    @Test
    public void pause___gameIsStarted___gameStateChangedToPaused() {
        when(game.getState()).thenReturn(Game.State.STARTED).thenCallRealMethod();

        game.pause();

        assertEquals(Game.State.PAUSED, game.getState());
    }

    @Test
    public void pause___gameIsStarted___gamePausedEventBroadcast() {
        when(game.getState()).thenReturn(Game.State.STARTED);

        game.pause();

        verify(eventBroadcaster).broadcast(any(GamePausedEvent.class));
    }

    @Test
    public void pause___gameIsStarted___countdownTimerIsPaused() {
        when(game.getState()).thenReturn(Game.State.STARTED);

        game.pause();

        verify(timer).pause();
    }

    @Test
    public void pause___gameIsAlreadyPaused___doNothing() {
        when(game.getState()).thenReturn(Game.State.STARTED).thenCallRealMethod();

        game.pause();
        game.pause();
        game.pause();

        verify(eventBroadcaster, times(1)).broadcast(any(GamePausedEvent.class));
        verify(timer, times(1)).pause();
    }

    @Test(expected = InvalidGameStateException.class)
    public void pause___gameIsAlreadyFinished_boardComplete___throwInvalidGameStateException() {
        when(game.getState()).thenReturn(Game.State.COMPLETED);
        game.pause();
    }

    @Test(expected = InvalidGameStateException.class)
    public void pause___gameIsAlreadyFinished_timerExpired___throwInvalidGameStateException() {
        when(game.getState()).thenReturn(Game.State.TIMER_EXPIRED);
        game.pause();
    }

    @Test
    public void restart___boardIsInit() {
        game.restart();
        verify(board).init(any(BoardSize.class));
    }

    @Test
    public void restart___timerIsResetAndStarted() {
        game.restart();
        verify(timer).reset();
        verify(timer).start();
    }

    @Test
    public void restart___trackedStatsIsReset() {
        game.restart();
        verify(statsTracker).reset();
    }

    @Test
    public void restart___gameStateChangedToStarted() {
        game.restart();
        assertEquals(Game.State.STARTED, game.getState());
    }

    @Test
    public void restart___gameRestartedEventBroadcast() {
        game.restart();
        verify(eventBroadcaster).broadcast(any(GameRestartedEvent.class));
    }

    @Test
    public void open___pending___numberOfAttemptsIsNotIncremented() {
        when(board.open(anyInt())).thenReturn(Board.OpenResult.PENDING);

        game.start();
        game.open(0);

        verify(statsTracker, never()).incrementNumberOfAttempts();
    }

    @Test
    public void open___match___numberOfMatchedTilesIsIncremented() {
        when(board.open(anyInt())).thenReturn(Board.OpenResult.MATCH);

        game.start();
        game.open(0);

        verify(statsTracker).incrementNumberOfMatchedTiles();
    }

    @Test
    public void open___match___numberOfAttemptsIsIncremented() {
        when(board.open(anyInt())).thenReturn(Board.OpenResult.MATCH);

        game.start();
        game.open(0);

        verify(statsTracker).incrementNumberOfAttempts();
    }

    @Test
    public void open___match___gameOpenMatchEventBroadcast() {
        when(board.open(anyInt())).thenReturn(Board.OpenResult.MATCH);

        game.start();
        game.open(0);

        ArgumentCaptor<Event> captor = ArgumentCaptor.forClass(Event.class);
        verify(eventBroadcaster, atLeastOnce()).broadcast(captor.capture());

        assertEquals(GameOpenMatchEvent.class, captor.getValue().getClass());
    }

    @Test
    public void open___noMatch___numberOfMatchedTilesIsNotIncremented() {
        when(board.open(anyInt())).thenReturn(Board.OpenResult.NO_MATCH);

        game.start();
        game.open(0);

        verify(statsTracker, never()).incrementNumberOfMatchedTiles();
    }

    @Test
    public void open___noMatch___numberOfAttemptsIsIncremented() {
        when(board.open(anyInt())).thenReturn(Board.OpenResult.NO_MATCH);

        game.start();
        game.open(0);

        verify(statsTracker).incrementNumberOfAttempts();
    }

    @Test
    public void open___noMatch___gameOpenNoMatchEventBroadcast() {
        when(board.open(anyInt())).thenReturn(Board.OpenResult.NO_MATCH);

        game.start();
        game.open(0);

        ArgumentCaptor<Event> captor = ArgumentCaptor.forClass(Event.class);
        verify(eventBroadcaster, atLeastOnce()).broadcast(captor.capture());

        assertEquals(GameOpenNoMatchEvent.class, captor.getValue().getClass());
    }

    @Test
    public void open___complete___numberOfMatchedTilesIsIncremented() {
        when(board.getBoardSize()).thenReturn(newBoardSize(4));
        when(board.open(anyInt())).thenReturn(Board.OpenResult.COMPLETE);

        game.start();
        game.open(0);

        verify(statsTracker).incrementNumberOfMatchedTiles();
    }

    @Test
    public void open___complete___numberOfAttemptsIsIncremented() {
        when(board.getBoardSize()).thenReturn(newBoardSize(4));
        when(board.open(anyInt())).thenReturn(Board.OpenResult.COMPLETE);

        game.start();
        game.open(0);

        verify(statsTracker).incrementNumberOfAttempts();
    }

    @Test(expected = InvalidGameStateException.class)
    public void open___gameStateInit___throwInvalidGameStateException() {
        game.open(0);
    }

    @Test(expected = InvalidGameStateException.class)
    public void open___gameStatePaused___throwInvalidGameStateException() {
        game.start();
        game.pause();
        game.open(0);
    }

    @Test(expected = InvalidGameStateException.class)
    public void open___gameStateCompleted___throwInvalidGameStateException() {
        when(board.getBoardSize()).thenReturn(newBoardSize(4));
        when(board.open(anyInt())).thenReturn(Board.OpenResult.COMPLETE);

        game.start();
        game.open(0);
        game.open(1);
    }

    @Test(expected = InvalidGameStateException.class)
    public void open___gameStateTimerExpired___throwInvalidGameStateException() {
        when(board.getBoardSize()).thenReturn(newBoardSize(4));

        ((CountdownTimer.Listener) game).onTimerExpired();

        game.open(0);
    }

    @Test
    public void closeOpenedTiles___tilesAreClosed() {
        game.closeOpenedTiles();
        verify(board).closeOpenedTiles();
    }

    @Test
    public void boardComplete___gameStateChangedToCompleted() {
        when(board.getBoardSize()).thenReturn(newBoardSize(4));
        when(board.open(anyInt())).thenReturn(Board.OpenResult.COMPLETE);

        game.start();
        game.open(0);

        assertEquals(Game.State.COMPLETED, game.getState());
    }

    @Test
    public void boardComplete___gameCompletedEventBroadcast() {
        when(board.getBoardSize()).thenReturn(newBoardSize(4));
        when(board.open(anyInt())).thenReturn(Board.OpenResult.COMPLETE);

        game.start();
        game.open(0);

        ArgumentCaptor<Event> captor = ArgumentCaptor.forClass(Event.class);
        verify(eventBroadcaster, atLeastOnce()).broadcast(captor.capture());

        assertEquals(GameCompletedEvent.class, captor.getValue().getClass());
    }

    @Test
    public void boardComplete___timerStopped() {
        when(board.getBoardSize()).thenReturn(newBoardSize(4));
        when(board.open(anyInt())).thenReturn(Board.OpenResult.COMPLETE);

        game.start();
        game.open(0);

        verify(timer).pause();
    }

    @Test
    public void timerTick___gameTimerTickEventBroadcast() {
        ((CountdownTimer.Listener) game).onTimerTick();
        verify(eventBroadcaster).broadcast(any(GameTimerTickEvent.class));
    }

    @Test
    public void timerExpired___gameStateChangedToTimerExpired() {
        when(board.getBoardSize()).thenReturn(newBoardSize(4));

        ((CountdownTimer.Listener) game).onTimerExpired();
        assertEquals(Game.State.TIMER_EXPIRED, game.getState());
    }

    @Test
    public void timerExpired___gameTimerExpiredEventBroadcast() {
        when(board.getBoardSize()).thenReturn(newBoardSize(4));

        ((CountdownTimer.Listener) game).onTimerExpired();
        verify(eventBroadcaster).broadcast(any(GameTimerExpiredEvent.class));
    }

    @Test
    public void timerReset___nothingHappens() {
        // Just to have 100% code coverage.
        ((CountdownTimer.Listener) game).onTimerReset();
    }

    @Test
    public void getGameSpecs___returnGameSpecs() {
        when(board.getBoardSize()).thenReturn(newBoardSize(4));
        when(timer.getDurationMillis()).thenReturn(3000L);

        assertEquals(4, game.getGameSpecs().getBoardSize().count());
        assertEquals(3000, game.getGameSpecs().getDurationMillis());
    }

    @Test
    public void isFinished___boardCompleted___returnTrue() {
        when(board.getBoardSize()).thenReturn(newBoardSize(4));
        when(board.open(anyInt())).thenReturn(Board.OpenResult.COMPLETE);

        game.start();
        game.open(0);

        assertTrue(game.isFinished());
    }

    @Test
    public void isFinished___timerExpired___returnTrue() {
        when(board.getBoardSize()).thenReturn(newBoardSize(4));

        ((CountdownTimer.Listener) game).onTimerExpired();

        assertTrue(game.isFinished());
    }

    @Test
    public void isFinished___boardNotCompletedAndTimerNotExpired___returnFalse() {
        assertFalse(game.isFinished());
    }

    @Test
    public void subscribe___eventBroadcastSubscribed() {
        EventBroadcaster.Subscription subscription = mock(EventBroadcaster.Subscription.class);
        when(eventBroadcaster.subscribe(any(), any())).thenReturn(subscription);

        assertEquals(subscription, game.subscribe(
                GameCompletedEvent.class, mock(GameCompletedEventHandler.class)));
    }

}
