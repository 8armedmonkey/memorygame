package eightam.memorygame.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestGameTimerTickEvent {

    @Test
    public void constructor_timeMillisAreSet() {
        GameTimerTickEvent event = new GameTimerTickEvent(1000, 3000);
        assertEquals(1000, event.getElapsedMillis());
        assertEquals(3000, event.getDurationMillis());
    }

}
