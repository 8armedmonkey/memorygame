package eightam.memorygame.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class TestGameCompletedEvent {

    @Test
    public void constructor_statsIsSet() {
        Stats stats = mock(Stats.class);
        assertEquals(stats, new GameCompletedEvent(stats).getStats());
    }

}
