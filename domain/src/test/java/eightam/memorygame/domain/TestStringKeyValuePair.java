package eightam.memorygame.domain;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TestStringKeyValuePair {

    @Test
    public void setKeyValue_keyValueSet() {
        StringKeyValuePair pair = new StringKeyValuePair("KEY", "VALUE");

        assertEquals("KEY", pair.getKey());
        assertEquals("VALUE", pair.getValue());
    }

}
