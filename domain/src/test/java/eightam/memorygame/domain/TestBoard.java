package eightam.memorygame.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import static eightam.memorygame.domain.BoardUtils.getTilesAsArray;
import static eightam.memorygame.domain.BoardUtils.groupTileIndicesByNumericValues;
import static eightam.memorygame.domain.Factories.newBoard;
import static eightam.memorygame.domain.Factories.newBoardSize;
import static eightam.memorygame.domain.TileUtils.expectTilesStatuses;
import static org.junit.Assert.*;

public class TestBoard {

    private Board board;

    @Before
    public void setUp() {
        board = newBoard();
    }

    @Test
    public void init___boardSizeEven___noInvalidBoardSizeExceptionThrown() {
        board.init(newBoardSize(4));
        assertEquals(4, board.getBoardSize().count());
    }

    @Test(expected = InvalidBoardSizeException.class)
    public void init___boardSizeOdd___throwInvalidBoardSizeException() {
        board.init(newBoardSize(1));
    }

    @Test(expected = InvalidBoardSizeException.class)
    public void init___boardSizeZero___throwInvalidBoardSizeException() {
        board.init(newBoardSize(0));
    }

    @Test(expected = InvalidBoardSizeException.class)
    public void init___boardSizeNegative___throwInvalidBoardSizeException() {
        board.init(newBoardSize(-4));
    }

    @Test
    public void init___boardSizeEven___tilesGeneratedInPairs() {
        Map<Integer, Integer> countNumericValues = new HashMap<>();
        board.init(newBoardSize(16));

        for (Tile tile : board) {
            int numericValue = tile.getNumericValue();
            int count = countNumericValues.getOrDefault(numericValue, 0);

            countNumericValues.put(numericValue, count + 1);
        }

        for (int count : countNumericValues.values()) {
            if (count != 2) {
                fail();
            }
        }
    }

    @Test
    public void init___boardSizeEven___tilesGeneratedRandomly() {
        board.init(newBoardSize(16));

        Iterator<Tile> iterator = board.iterator();
        int count = 0;

        while (iterator.hasNext()) {
            Tile t1 = iterator.next();
            Tile t2 = iterator.next();

            if (t1.getNumericValue() != t2.getNumericValue()) {
                count++;
            }
        }

        assertNotEquals(0, count);
    }

    @Test(expected = BoardNotInitializedException.class)
    public void getBoardSize___hasNotBeenInit___throwBoardNotInitializedException() {
        board.getBoardSize();
    }

    @Test
    public void open___oneTile___openResultPending() {
        board.init(newBoardSize(4));
        assertEquals(Board.OpenResult.PENDING, board.open(2));
    }

    @Test
    public void open___twoTilesNoMatch___openResultNoMatch() {
        board.init(newBoardSize(4));

        Map<Integer, IntPair> tileIndicesByNumericValues = groupTileIndicesByNumericValues(board);
        Iterator<IntPair> iterator = tileIndicesByNumericValues.values().iterator();

        IntPair pair1 = iterator.next();
        IntPair pair2 = iterator.next();

        board.open(pair1.getFirst());
        assertEquals(Board.OpenResult.NO_MATCH, board.open(pair2.getFirst()));
    }

    @Test
    public void open___twoTilesNoMatch___tilesStatusesAreSetToOpen() {
        board.init(newBoardSize(4));

        Map<Integer, IntPair> tileIndicesByNumericValues = groupTileIndicesByNumericValues(board);
        Iterator<IntPair> iterator = tileIndicesByNumericValues.values().iterator();

        IntPair pair1 = iterator.next();
        IntPair pair2 = iterator.next();

        board.open(pair1.getFirst());
        board.open(pair2.getFirst());

        Tile[] tiles = getTilesAsArray(board);
        assertTrue(expectTilesStatuses(Tile.Status.OPEN, tiles[pair1.getFirst()], tiles[pair2.getFirst()]));
    }

    @Test
    public void open___twoTilesMatch___openResultMatch() {
        board.init(newBoardSize(4));

        Map<Integer, IntPair> tileIndicesByNumericValues = groupTileIndicesByNumericValues(board);
        Iterator<IntPair> iterator = tileIndicesByNumericValues.values().iterator();

        IntPair pair = iterator.next();

        board.open(pair.getFirst());
        assertEquals(Board.OpenResult.MATCH, board.open(pair.getSecond()));
    }

    @Test
    public void open___allTiles___openResultComplete() {
        board.init(newBoardSize(4));

        Map<Integer, IntPair> tileIndicesByNumericValues = groupTileIndicesByNumericValues(board);
        Iterator<IntPair> iterator = tileIndicesByNumericValues.values().iterator();

        IntPair pair1 = iterator.next();
        IntPair pair2 = iterator.next();

        board.open(pair1.getFirst());
        board.open(pair1.getSecond());
        board.open(pair2.getFirst());
        assertEquals(Board.OpenResult.COMPLETE, board.open(pair2.getSecond()));
    }

    @Test(expected = InvalidTileException.class)
    public void open___tileIndexOutOfRangeNegative___throwInvalidTileException() {
        board.init(newBoardSize(4));
        board.open(-1);
    }

    @Test(expected = InvalidTileException.class)
    public void open___tileIndexOutOfRangePositive___throwInvalidTileException() {
        board.init(newBoardSize(4));
        board.open(4);
    }

    @Test
    public void open___tileIndexWithinRange___noThrowInvalidTileException() {
        board.init(newBoardSize(4));

        try {
            board.open(0);
        } catch (InvalidTileException e) {
            fail();
        }
    }

    @Test(expected = InvalidTileException.class)
    public void open___tileIsAlreadyOpen___throwInvalidTileException() {
        board.init(newBoardSize(4));
        board.open(0);
        board.open(0);
    }

    @Test(expected = InvalidTileException.class)
    public void open___tileIsAlreadyMatched___throwInvalidTileException() {
        board.init(newBoardSize(4));

        Map<Integer, IntPair> tileIndicesByNumericValues = groupTileIndicesByNumericValues(board);
        Iterator<IntPair> iterator = tileIndicesByNumericValues.values().iterator();

        IntPair pair = iterator.next();

        board.open(pair.getFirst());
        board.open(pair.getSecond());
        board.open(pair.getSecond());
    }

    @Test(expected = InvalidOpenException.class)
    public void open___moreThanTwoTilesAtOnce___throwInvalidOpenException() {
        board.init(newBoardSize(4));

        Map<Integer, IntPair> tileIndicesByNumericValues = groupTileIndicesByNumericValues(board);
        Iterator<IntPair> iterator = tileIndicesByNumericValues.values().iterator();

        IntPair pair1 = iterator.next();
        IntPair pair2 = iterator.next();

        board.open(pair1.getFirst());
        board.open(pair2.getFirst());
        board.open(pair2.getSecond());
    }

    @Test
    public void closeOpenedTiles___openedTilesAreClosed() {
        board.init(newBoardSize(4));

        Map<Integer, IntPair> tileIndicesByNumericValues = groupTileIndicesByNumericValues(board);
        Iterator<IntPair> iterator = tileIndicesByNumericValues.values().iterator();

        IntPair pair1 = iterator.next();
        IntPair pair2 = iterator.next();

        board.open(pair1.getFirst());
        board.open(pair2.getFirst());
        board.closeOpenedTiles();

        assertTrue(expectTilesStatuses(Tile.Status.CLOSE, getTilesAsArray(board)));
    }

    @Test
    public void closeOpenedTiles___nextOpenWontThrowInvalidOpenException() {
        board.init(newBoardSize(4));

        Map<Integer, IntPair> tileIndicesByNumericValues = groupTileIndicesByNumericValues(board);
        Iterator<IntPair> iterator = tileIndicesByNumericValues.values().iterator();

        IntPair pair1 = iterator.next();
        IntPair pair2 = iterator.next();

        board.open(pair1.getFirst());
        board.open(pair2.getFirst());
        board.closeOpenedTiles();

        try {
            board.open(pair1.getSecond());
        } catch (InvalidOpenException e) {
            fail();
        }
    }

}
