package eightam.memorygame.domain;

import org.junit.Before;
import org.junit.Test;

import static eightam.memorygame.domain.Factories.newScoreEntryBuilder;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;

public class TestScoreEntry {

    private ScoreEntry.Builder builder;

    @Before
    public void setUp() {
        builder = newScoreEntryBuilder();
    }

    @Test
    public void score_scoreIsSet() {
        Score score = mock(Score.class);
        ScoreEntry entry = builder.score(score).build();

        assertEquals(score, entry.getScore());
    }

    @Test
    public void scoreData_scoreDataIsSet() {
        ScoreData scoreData = mock(ScoreData.class);
        ScoreEntry entry = builder.scoreData(scoreData).build();

        assertEquals(scoreData, entry.getScoreData());
    }

}
