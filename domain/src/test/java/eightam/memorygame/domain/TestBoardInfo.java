package eightam.memorygame.domain;

import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static eightam.memorygame.domain.Factories.newBoardSize;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestBoardInfo {

    private Board board;
    private BoardInfo boardInfo;

    @Before
    public void setUp() {
        board = mock(Board.class);
        boardInfo = new BoardInfoImpl(board);
    }

    @Test
    public void getBoardSize_boardSizeIsReturned() {
        when(board.getBoardSize()).thenReturn(newBoardSize(4));
        assertEquals(4, boardInfo.getBoardSize().count());
    }

    @Test
    public void isBoardComplete_boardIsNotComplete_returnFalse() {
        when(board.isBoardComplete()).thenReturn(false);
        assertFalse(boardInfo.isBoardComplete());
    }

    @Test
    public void isBoardComplete_boardIsComplete_returnTrue() {
        when(board.isBoardComplete()).thenReturn(true);
        assertTrue(boardInfo.isBoardComplete());
    }

    @Test
    public void getTileInfoIterator_tileInfoIteratorIsReturned() {
        List<Tile> tiles = new ArrayList<>();
        tiles.add(new TileImpl(0, Tile.Status.MATCH));
        tiles.add(new TileImpl(1, Tile.Status.OPEN));
        tiles.add(new TileImpl(1, Tile.Status.CLOSE));
        tiles.add(new TileImpl(0, Tile.Status.MATCH));

        when(board.iterator()).thenReturn(tiles.iterator());

        TileInfo tf;

        tf = boardInfo.getTileInfoIterator().next();
        assertEquals(0, tf.getNumericValue());
        assertEquals(Tile.Status.MATCH, tf.getStatus());

        tf = boardInfo.getTileInfoIterator().next();
        assertEquals(1, tf.getNumericValue());
        assertEquals(Tile.Status.OPEN, tf.getStatus());

        tf = boardInfo.getTileInfoIterator().next();
        assertEquals(1, tf.getNumericValue());
        assertEquals(Tile.Status.CLOSE, tf.getStatus());

        tf = boardInfo.getTileInfoIterator().next();
        assertEquals(0, tf.getNumericValue());
        assertEquals(Tile.Status.MATCH, tf.getStatus());
    }

}
