package eightam.memorygame.domain;

import org.junit.Test;

import java.lang.reflect.InvocationTargetException;

import static eightam.memorygame.domain.Factories.newBoardSize;
import static eightam.memorygame.util.AssertionUtils.assertUtilityClassWellDefined;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestStatsUtils {

    @Test
    public void wellDefinedUtilityClass() {
        try {
            assertUtilityClassWellDefined(StatsUtils.class);

        } catch (NoSuchMethodException
                | InvocationTargetException
                | InstantiationException
                | IllegalAccessException e) {
            fail();
        }
    }

    @Test
    public void build___statsValues___statsBuilt() {
        Board board = mock(Board.class);
        CountdownTimer timer = mock(CountdownTimer.class);
        StatsTracker statsTracker = mock(StatsTracker.class);

        when(board.getBoardSize()).thenReturn(newBoardSize(4));
        when(timer.getDurationMillis()).thenReturn(3000L);
        when(timer.getElapsedMillis()).thenReturn(1000L);
        when(statsTracker.getNumberOfAttempts()).thenReturn(10);
        when(statsTracker.getNumberOfMatchedTiles()).thenReturn(2);

        Stats stats = StatsUtils.buildStats(board, timer, statsTracker);

        assertEquals(4, stats.getNumberOfTiles());
        assertEquals(3000L, stats.getDurationMillis());
        assertEquals(1000L, stats.getDurationToCompleteMillis());
        assertEquals(10, stats.getNumberOfAttempts());
        assertEquals(2, stats.getNumberOfMatchedTiles());
    }

}
