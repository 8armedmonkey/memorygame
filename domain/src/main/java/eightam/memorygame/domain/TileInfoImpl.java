package eightam.memorygame.domain;

class TileInfoImpl implements TileInfo {

    private final Tile tile;

    TileInfoImpl(Tile tile) {
        this.tile = tile;
    }

    @Override
    public int getNumericValue() {
        return tile.getNumericValue();
    }

    @Override
    public Tile.Status getStatus() {
        return tile.getStatus();
    }

}
