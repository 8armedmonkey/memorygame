package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface ScoreEntries extends Iterable<ScoreEntry> {

    /**
     * Get the size of the collection.
     *
     * @return Size of the collection.
     */
    @Api
    int size();

}
