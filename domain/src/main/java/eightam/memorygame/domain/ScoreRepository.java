package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface ScoreRepository {

    /**
     * Retrieve scores specified by range.
     *
     * @param range The range of scores.
     * @return ScoreEntries.
     */
    @Api
    ScoreEntries retrieve(IntRange range);

    /**
     * Add a new score to the repository.
     *
     * @param scoreEntry The score entry to be added.
     */
    @Api
    void add(ScoreEntry scoreEntry);

    /**
     * Clear all scores from the repository.
     */
    @Api
    void clear();

}
