package eightam.memorygame.domain;

class StatsImpl implements Stats {

    private final int numberOfTiles;
    private final int numberOfMatchedTiles;
    private final int numberOfAttempts;
    private final long durationMillis;
    private final long durationToCompleteMillis;

    StatsImpl(
            int numberOfTiles,
            int numberOfMatchedTiles,
            int numberOfAttempts,
            long durationMillis,
            long durationToCompleteMillis) throws InvalidStatsException {

        assertNumbersOfTilesAndMatchedTilesValid(numberOfTiles, numberOfMatchedTiles);
        assertNumberOfAttemptsValid(numberOfMatchedTiles, numberOfAttempts);
        assertDurationAndDurationToCompleteValid(durationMillis, durationToCompleteMillis);

        this.numberOfTiles = numberOfTiles;
        this.numberOfMatchedTiles = numberOfMatchedTiles;
        this.numberOfAttempts = numberOfAttempts;
        this.durationMillis = durationMillis;
        this.durationToCompleteMillis = durationToCompleteMillis;
    }

    @Override
    public int getNumberOfTiles() {
        return numberOfTiles;
    }

    @Override
    public int getNumberOfMatchedTiles() {
        return numberOfMatchedTiles;
    }

    @Override
    public boolean isCompleted() {
        return numberOfTiles == numberOfMatchedTiles;
    }

    @Override
    public double getCompletenessPercentage() {
        return (double) numberOfMatchedTiles / numberOfTiles;
    }

    @Override
    public int getNumberOfAttempts() {
        return numberOfAttempts;
    }

    @Override
    public double getAttemptsPercentage() {
        double bestPossibleAttempts = numberOfTiles / 2;
        return bestPossibleAttempts / numberOfAttempts;
    }

    @Override
    public long getDurationMillis() {
        return durationMillis;
    }

    @Override
    public long getDurationToCompleteMillis() {
        return durationToCompleteMillis;
    }

    @Override
    public double getDurationPercentage() {
        return 1.0 - ((double) durationToCompleteMillis / durationMillis);
    }

    private static void assertNumbersOfTilesAndMatchedTilesValid(
            int numberOfTiles, int numberOfMatchedTiles) throws InvalidStatsException {

        if (numberOfTiles < numberOfMatchedTiles) {
            throw new InvalidStatsException();
        }
    }

    private static void assertNumberOfAttemptsValid(
            int numberOfMatchedTiles, int numberOfAttempts) throws InvalidStatsException {

        if (numberOfAttempts < numberOfMatchedTiles / 2) {
            throw new InvalidStatsException();
        }
    }

    private static void assertDurationAndDurationToCompleteValid(
            long durationMillis, long durationToCompleteMillis) throws InvalidStatsException {

        if (durationMillis < durationToCompleteMillis) {
            throw new InvalidStatsException();
        }
    }

    static class Builder implements Stats.Builder {

        private int numberOfTiles;
        private int numberOfMatchedTiles;
        private int numberOfAttempts;
        private long durationMillis;
        private long durationToCompleteMillis;

        @Override
        public Stats.Builder numberOfTiles(int numberOfTiles) {
            this.numberOfTiles = numberOfTiles;
            return this;
        }

        @Override
        public Stats.Builder numberOfMatchedTiles(int numberOfMatchedTiles) {
            this.numberOfMatchedTiles = numberOfMatchedTiles;
            return this;
        }

        @Override
        public Stats.Builder numberOfAttempts(int numberOfAttempts) {
            this.numberOfAttempts = numberOfAttempts;
            return this;
        }

        @Override
        public Stats.Builder durationMillis(long durationMillis) {
            this.durationMillis = durationMillis;
            return this;
        }

        @Override
        public Stats.Builder durationToCompleteMillis(long durationToCompleteMillis) {
            this.durationToCompleteMillis = durationToCompleteMillis;
            return this;
        }

        @Override
        public Stats build() {
            return new StatsImpl(
                    numberOfTiles,
                    numberOfMatchedTiles,
                    numberOfAttempts,
                    durationMillis,
                    durationToCompleteMillis);
        }

    }

}
