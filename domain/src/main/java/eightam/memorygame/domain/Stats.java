package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface Stats {

    /**
     * Get the number of tiles in the board.
     *
     * @return Number of tiles in the board.
     */
    @Api
    int getNumberOfTiles();

    /**
     * Get the number of matched tiles.
     *
     * @return Number of matched tiles.
     */
    @Api
    int getNumberOfMatchedTiles();

    /**
     * Get the completion status of the board.
     *
     * @return True if the board is completed, false otherwise.
     */
    @Api
    boolean isCompleted();

    /**
     * Get the completeness percentage (number of matched tiles vs. number of tiles).
     *
     * @return Completeness percentage.
     */
    @Api
    double getCompletenessPercentage();

    /**
     * Get the number of attempts made in completing the board.
     * The number of attempts are still counted even if the board is not completed in the end.
     *
     * @return Number of attempts to complete the board.
     */
    @Api
    int getNumberOfAttempts();

    /**
     * Get the attempts percentage (actual attempts made vs. best possible number of attempts).
     * The best possible number of attempts will be half the number of tiles, which means that
     * every attempt results in immediate match.
     *
     * @return Attempts percentage.
     */
    @Api
    double getAttemptsPercentage();

    /**
     * Get the maximum duration given to complete the board.
     *
     * @return Maximum duration in milliseconds.
     */
    @Api
    long getDurationMillis();

    /**
     * Get the duration required to complete the board.
     * Duration is still counted even if the board is not completed in the end.
     *
     * @return Duration to complete in milliseconds.
     */
    @Api
    long getDurationToCompleteMillis();

    /**
     * Get the duration percentage (duration to complete vs total duration).
     *
     * @return Duration percentage.
     */
    @Api
    double getDurationPercentage();

    @Api
    interface Builder {

        /**
         * Set the number of tiles in the board.
         *
         * @param numberOfTiles Number of tiles in the board.
         * @return {@link Builder} instance.
         */
        @Api
        Builder numberOfTiles(int numberOfTiles);

        /**
         * Set the number of matched tiles in the board.
         *
         * @param numberOfMatchedTiles Number of matched tiles in the board.
         * @return {@link} Builder instance.
         */
        @Api
        Builder numberOfMatchedTiles(int numberOfMatchedTiles);

        /**
         * Set the number of attempts made in completing the board.
         *
         * @param numberOfAttempts Number of attempts to complete the board.
         * @return {@link Builder} instance.
         */
        @Api
        Builder numberOfAttempts(int numberOfAttempts);

        /**
         * Set the maximum duration given to complete the board.
         *
         * @param durationMillis Maximum duration in milliseconds.
         * @return {@link Builder} instance.
         */
        @Api
        Builder durationMillis(long durationMillis);

        /**
         * Set the duration to complete the board.
         *
         * @param durationToCompleteMillis Duration to complete in milliseconds.
         * @return {@link Builder} instance.
         */
        @Api
        Builder durationToCompleteMillis(long durationToCompleteMillis);

        /**
         * Build the {@link Stats} instance.
         *
         * @return {@link Stats} instance.
         */
        @Api
        Stats build();

    }

}
