package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;
import sys.event.Event;

@Api
public final class GameTimerExpiredEvent implements Event {

    private final Stats stats;

    @Api
    public GameTimerExpiredEvent(Stats stats) {
        this.stats = stats;
    }

    @Api
    public Stats getStats() {
        return stats;
    }

}
