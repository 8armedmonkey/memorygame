package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface Score {

    /**
     * Get the score value.
     *
     * @return Score value as decimal number.
     */
    @Api
    double getValue();

}
