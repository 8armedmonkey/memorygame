package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;
import sys.event.Event;
import sys.event.EventBroadcaster;
import sys.event.EventHandler;

@Api
public interface Game {

    /**
     * Start the game, or resume the game if it is paused.
     */
    @Api
    void start() throws InvalidGameStateException;

    /**
     * Pause the game.
     */
    @Api
    void pause() throws InvalidGameStateException;

    /**
     * Restart the game.
     */
    @Api
    void restart();

    /**
     * Open a tile specified by the index.
     *
     * @param tileIndex The index of the tile within the board.
     * @throws InvalidGameStateException If the game is not in the {@link Game.State#STARTED} state.
     * @throws InvalidTileException      If the tile does not exist, is already opened or matched.
     * @throws InvalidOpenException      If an attempt to open more than two tiles at once is made.
     */
    @Api
    void open(int tileIndex) throws InvalidGameStateException, InvalidTileException, InvalidOpenException;

    /**
     * Close opened tiles that do not match.
     * Opened tiles that do not match have to be closed first before another open is made.
     */
    @Api
    void closeOpenedTiles();

    /**
     * Get the game specification.
     *
     * @return Game specification.
     */
    @Api
    GameSpecs getGameSpecs();

    /**
     * Get the current state of the game.
     *
     * @return Current game state.
     */
    @Api
    State getState();

    /**
     * Check if the game is finished or not.
     * The game is finished when the board is completed or the timer is expired.
     *
     * @return True if the game is finished, false otherwise.
     */
    @Api
    boolean isFinished();

    /**
     * Subscribe to a particular game event type.
     *
     * @param eventClass   Event class.
     * @param eventHandler Event handler.
     * @param <E>          Event type.
     * @return Subscription to the event.
     */
    @Api
    <E extends Event> EventBroadcaster.Subscription subscribe(Class<E> eventClass, EventHandler<E> eventHandler);

    /**
     * Game states:
     * <ul>
     * <li>INIT: game is initialized and has never been started.</li>
     * <li>STARTED: game is started.</li>
     * <li>PAUSED: game is paused.</li>
     * <li>COMPLETED: game is completed (all tiles are matched).</li>
     * <li>TIMER_EXPIRED: game has ended because the timer has expired.</li>
     * </ul>
     */
    @Api
    enum State {

        INIT, STARTED, PAUSED, COMPLETED, TIMER_EXPIRED

    }

}
