package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface ScoreData extends Iterable<StringKeyValuePair> {

    /**
     * Put the key-value pair in the data bundle.
     *
     * @param keyValuePair Key-value pair.
     */
    @Api
    void put(StringKeyValuePair keyValuePair);

    /**
     * Remove the key-value pair from the data bundle.
     *
     * @param key Key of the pair that is going to be removed.
     */
    @Api
    void remove(String key);

    /**
     * Check if the data contains the key.
     *
     * @param key Key that is being searched.
     * @return True if the entry with the key exists, false otherwise.
     */
    @Api
    boolean contains(String key);

    /**
     * Get the size of the details.
     *
     * @return Size of the details.
     */
    @Api
    int size();

    @Api
    interface Builder {

        /**
         * Put key-value pair for the score data.
         *
         * @param keyValuePair Key-value pair.
         * @return {@link Builder} instance.
         */
        @Api
        Builder put(StringKeyValuePair keyValuePair);

        /**
         * Create new instance of {@link ScoreData}.
         *
         * @return New {@link ScoreData} instance.
         */
        @Api
        ScoreData build();

    }

}
