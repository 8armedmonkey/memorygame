package eightam.memorygame.domain;

class TileImpl implements Tile {

    private final int numericValue;
    private Status status;

    TileImpl(int numericValue, Status status) {
        this.numericValue = numericValue;
        this.status = status;
    }

    @Override
    public int getNumericValue() {
        return numericValue;
    }

    @Override
    public Status getStatus() {
        return status;
    }

    @Override
    public void setStatus(Status status) {
        this.status = status;
    }

}
