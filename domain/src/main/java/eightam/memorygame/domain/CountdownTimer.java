package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface CountdownTimer {

    /**
     * Start the timer.
     */
    @Api
    void start();

    /**
     * Pause the timer.
     */
    @Api
    void pause();

    /**
     * Reset the timer.
     */
    @Api
    void reset();

    /**
     * Get the number of milliseconds since the timer is started for the first time.
     *
     * @return Timer elapsed time in milliseconds.
     */
    @Api
    long getElapsedMillis();

    /**
     * Get the timer duration in milliseconds.
     *
     * @return Timer duration in milliseconds.
     */
    @Api
    long getDurationMillis();

    /**
     * Check if the timer is currently running.
     *
     * @return True if running, false otherwise.
     */
    @Api
    boolean isRunning();

    /**
     * Check if the timer has expired or not.
     *
     * @return True if expired, false otherwise.
     */
    @Api
    boolean isExpired();

    /**
     * Set the listener for the timer event.
     *
     * @param listener The listener for the timer event.
     */
    @Api
    void setListener(Listener listener);

    @Api
    interface Listener {

        /**
         * Notify about a timer tick event.
         */
        @Api
        void onTimerTick();

        /**
         * Notify about a timer expired event.
         */
        @Api
        void onTimerExpired();

        /**
         * Notify about a timer reset event.
         */
        @Api
        void onTimerReset();

    }

}
