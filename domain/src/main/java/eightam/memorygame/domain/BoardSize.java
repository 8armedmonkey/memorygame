package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface BoardSize {

    @Api
    int count();

}
