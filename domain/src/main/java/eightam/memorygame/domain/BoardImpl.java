package eightam.memorygame.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static eightam.memorygame.domain.TileUtils.expectTilesStatuses;
import static eightam.memorygame.domain.TileUtils.setTilesStatuses;

class BoardImpl implements Board {

    private BoardSize boardSize;
    private List<Tile> tiles;
    private IntPair selection;

    BoardImpl() {
        tiles = new ArrayList<>();
        selection = new IntPairImpl();
    }

    @Override
    public synchronized Iterator<Tile> iterator() {
        return tiles.iterator();
    }

    @Override
    public synchronized void init(BoardSize size) throws InvalidBoardSizeException {
        assertValidBoardSize(size);
        boardSize = size;

        List<Tile> newTiles = new ArrayList<>();

        for (int i = 0, n = size.count() / 2; i < n; i++) {
            newTiles.add(new TileImpl(i, Tile.Status.CLOSE));
            newTiles.add(new TileImpl(i, Tile.Status.CLOSE));
        }

        Collections.shuffle(newTiles);
        tiles = newTiles;
    }

    @Override
    public synchronized BoardSize getBoardSize() throws BoardNotInitializedException {
        assertBoardSizeNotNull(boardSize);
        return boardSize;
    }

    @Override
    public synchronized OpenResult open(int tileIndex) throws InvalidTileException, InvalidOpenException {
        assertTileIndexWithinRange(tiles, tileIndex);
        assertSecondSelectionNotMadeYet(selection);

        Tile tile = tiles.get(tileIndex);
        assertTileIsNotOpenedOrMatchedYet(tile);

        selection.put(tileIndex);

        if (selection.isSecondSet()) {
            Tile first = tiles.get(selection.getFirst());
            Tile second = tiles.get(selection.getSecond());

            if (first.getNumericValue() == second.getNumericValue()) {
                setTilesStatuses(Tile.Status.MATCH, first, second);
                selection.clear();

                return isBoardComplete() ? OpenResult.COMPLETE : OpenResult.MATCH;
            } else {
                setTilesStatuses(Tile.Status.OPEN, first, second);
                return OpenResult.NO_MATCH;
            }
        } else {
            setTilesStatuses(Tile.Status.OPEN, tile);
            return OpenResult.PENDING;
        }
    }

    @Override
    public synchronized void closeOpenedTiles() {
        if (selection.isSecondSet()) {
            Tile second = tiles.get(selection.getSecond());
            second.setStatus(Tile.Status.CLOSE);
        }

        if (selection.isFirstSet()) {
            Tile first = tiles.get(selection.getFirst());
            first.setStatus(Tile.Status.CLOSE);
        }

        selection.clear();
    }

    @Override
    public synchronized boolean isBoardComplete() {
        return expectTilesStatuses(Tile.Status.MATCH, tiles.toArray(new Tile[tiles.size()]));
    }

    private static void assertValidBoardSize(BoardSize size) throws InvalidBoardSizeException {
        if (size.count() <= 0 || size.count() % 2 != 0) {
            throw new InvalidBoardSizeException();
        }
    }

    private static void assertBoardSizeNotNull(BoardSize size) throws BoardNotInitializedException {
        if (size == null) {
            throw new BoardNotInitializedException();
        }
    }

    private static void assertSecondSelectionNotMadeYet(IntPair selection) throws InvalidOpenException {
        if (selection.isSecondSet()) {
            throw new InvalidOpenException();
        }
    }

    private static void assertTileIndexWithinRange(List<Tile> tiles, int tileIndex) {
        if (tileIndex < 0 || tileIndex >= tiles.size()) {
            throw new InvalidTileException();
        }
    }

    private static void assertTileIsNotOpenedOrMatchedYet(Tile tile) throws InvalidTileException {
        if (Tile.Status.OPEN == tile.getStatus() || Tile.Status.MATCH == tile.getStatus()) {
            throw new InvalidTileException();
        }
    }

}
