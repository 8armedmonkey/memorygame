package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface GameSpecs {

    /**
     * Get the board size for the game.
     *
     * @return Game board size.
     */
    @Api
    BoardSize getBoardSize();

    /**
     * Get the duration for the game in milliseconds.
     *
     * @return Game duration.
     */
    @Api
    long getDurationMillis();

    @Api
    interface Builder {

        /**
         * Set the board size for the game.
         *
         * @param boardSize Game board size
         * @return {@link Builder} instance.
         */
        @Api
        Builder boardSize(BoardSize boardSize);

        /**
         * Set the duration for the game in milliseconds.
         *
         * @param durationMillis Game duration.
         * @return {@link Builder} instance.
         */
        @Api
        Builder durationMillis(long durationMillis);

        /**
         * Create a new {@link GameSpecs} instance.
         *
         * @return {@link GameSpecs} instance.
         */
        @Api
        GameSpecs build();

    }

}
