package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;
import sys.event.Event;

@Api
public final class GameCompletedEvent implements Event {

    private final Stats stats;

    @Api
    public GameCompletedEvent(Stats stats) {
        this.stats = stats;
    }

    @Api
    public Stats getStats() {
        return stats;
    }

}
