package eightam.memorygame.domain;

class ScoreImpl implements Score {

    private final double value;

    ScoreImpl(double value) {
        this.value = value;
    }

    @Override
    public double getValue() {
        return value;
    }

}
