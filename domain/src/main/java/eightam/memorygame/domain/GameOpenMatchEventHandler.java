package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;
import sys.event.EventHandler;

@Api
public interface GameOpenMatchEventHandler extends EventHandler<GameOpenMatchEvent> {
}
