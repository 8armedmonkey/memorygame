package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public class StringKeyValuePair implements KeyValuePair<String, String> {

    private final String key;
    private final String value;

    @Api
    public StringKeyValuePair(String key, String value) {
        this.key = key;
        this.value = value;
    }

    @Api
    @Override
    public String getKey() {
        return key;
    }

    @Api
    @Override
    public String getValue() {
        return value;
    }

}
