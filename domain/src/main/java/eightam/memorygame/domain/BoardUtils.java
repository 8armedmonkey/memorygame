package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Api
public final class BoardUtils {

    private BoardUtils() {
    }

    @Api
    public static Map<Integer, IntPair> groupTileIndicesByNumericValues(Board board) {
        Map<Integer, IntPair> tileIndicesByNumericValues = new HashMap<>();
        int index = 0;

        for (Tile tile : board) {
            int numericValue = tile.getNumericValue();
            IntPair pair = tileIndicesByNumericValues.getOrDefault(numericValue, new IntPairImpl());

            pair.put(index);
            tileIndicesByNumericValues.put(numericValue, pair);

            index++;
        }
        return tileIndicesByNumericValues;
    }

    @Api
    public static Tile[] getTilesAsArray(Board board) {
        List<Tile> tiles = new ArrayList<>();

        for (Tile tile : board) {
            tiles.add(tile);
        }
        return tiles.toArray(new Tile[tiles.size()]);
    }

}
