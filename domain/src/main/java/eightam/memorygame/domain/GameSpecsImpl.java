package eightam.memorygame.domain;

class GameSpecsImpl implements GameSpecs {

    private final BoardSize boardSize;
    private final long durationMillis;

    GameSpecsImpl(BoardSize boardSize, long durationMillis) {
        this.boardSize = boardSize;
        this.durationMillis = durationMillis;
    }

    @Override
    public BoardSize getBoardSize() {
        return boardSize;
    }

    @Override
    public long getDurationMillis() {
        return durationMillis;
    }

    static class Builder implements GameSpecs.Builder {

        private BoardSize boardSize;
        private long durationMillis;

        @Override
        public GameSpecs.Builder boardSize(BoardSize boardSize) {
            this.boardSize = boardSize;
            return this;
        }

        @Override
        public GameSpecs.Builder durationMillis(long durationMillis) {
            this.durationMillis = durationMillis;
            return this;
        }

        @Override
        public GameSpecs build() {
            return new GameSpecsImpl(boardSize, durationMillis);
        }

    }

}
