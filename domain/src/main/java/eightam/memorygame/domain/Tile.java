package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface Tile {

    /**
     * Get the numeric value associated with this tile.
     *
     * @return The tile numeric value.
     */
    @Api
    int getNumericValue();

    /**
     * Get the status of the tile.
     *
     * @return The tile status.
     */
    @Api
    Status getStatus();

    /**
     * Set the status of the tile.
     *
     * @param status The tile status.
     */
    @Api
    void setStatus(Status status);

    /**
     * The status of the tile.
     * <p>
     * <ul>
     * <li>CLOSE: the tile is closed.</li>
     * <li>OPEN: the tile is opened.</li>
     * <li>MATCH: the tile is opened and matched with other tile.</li>
     * </ul>
     */
    @Api
    enum Status {

        CLOSE, OPEN, MATCH

    }

}
