package eightam.memorygame.domain;

class ScoreEntryImpl implements ScoreEntry {

    private final Score score;
    private final ScoreData scoreData;

    ScoreEntryImpl(Score score, ScoreData scoreData) {
        this.score = score;
        this.scoreData = scoreData;
    }

    @Override
    public Score getScore() {
        return score;
    }

    @Override
    public ScoreData getScoreData() {
        return scoreData;
    }

    static class Builder implements ScoreEntry.Builder {

        private Score score;
        private ScoreData scoreData;

        @Override
        public ScoreEntry.Builder score(Score score) {
            this.score = score;
            return this;
        }

        @Override
        public ScoreEntry.Builder scoreData(ScoreData scoreData) {
            this.scoreData = scoreData;
            return this;
        }

        @Override
        public ScoreEntry build() {
            return new ScoreEntryImpl(score, scoreData);
        }

    }

}
