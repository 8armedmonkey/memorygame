package eightam.memorygame.domain;

interface IntPair extends Iterable<Integer> {

    /**
     * Get the first number in the pair.
     *
     * @return The first number.
     * @throws ValueNotYetSetException If the first number has not been set.
     */
    int getFirst() throws ValueNotYetSetException;

    /**
     * Get the second number in the pair.
     *
     * @return The second number.
     * @throws ValueNotYetSetException If the second number has not been set.
     */
    int getSecond() throws ValueNotYetSetException;

    /**
     * Put a number into the pair.
     *
     * @param number A number.
     * @throws MaxCapacityReachedException If the max capacity of the pair has been reached.
     */
    void put(int number) throws MaxCapacityReachedException;

    /**
     * Remove the first number from the pair.
     * If the second number is set, it will become the first.
     *
     * @throws ValueNotYetSetException If the first number has not been set.
     */
    void removeFirst() throws ValueNotYetSetException;

    /**
     * Remove the second number from the pair.
     *
     * @throws ValueNotYetSetException If the second number has not been set.
     */
    void removeSecond() throws ValueNotYetSetException;

    /**
     * Clear the numbers from the pair.
     */
    void clear();

    /**
     * Check if the first number in the pair is set.
     *
     * @return True if the first number is set, false otherwise.
     */
    boolean isFirstSet();

    /**
     * Check if the second number in the pair is set.
     *
     * @return True if the second number is set, false otherwise.
     */
    boolean isSecondSet();

    /**
     * Check if the pair is empty.
     *
     * @return True if it is empty, false otherwise.
     */
    boolean isEmpty();

}
