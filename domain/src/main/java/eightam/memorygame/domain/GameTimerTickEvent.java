package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;
import sys.event.Event;

@Api
public final class GameTimerTickEvent implements Event {

    private final long elapsedMillis;
    private final long durationMillis;

    public GameTimerTickEvent(long elapsedMillis, long durationMillis) {
        this.elapsedMillis = elapsedMillis;
        this.durationMillis = durationMillis;
    }

    public long getElapsedMillis() {
        return elapsedMillis;
    }

    public long getDurationMillis() {
        return durationMillis;
    }

}
