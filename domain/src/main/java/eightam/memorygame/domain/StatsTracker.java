package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface StatsTracker {

    /**
     * Increment the number of attempts made in completing the board.
     */
    @Api
    void incrementNumberOfAttempts();

    /**
     * Increment the number of tiles being matched each time.
     * Increment is always by 2.
     */
    @Api
    void incrementNumberOfMatchedTiles();

    /**
     * Get the number of matched tiles.
     *
     * @return Number of matched tiles.
     */
    @Api
    int getNumberOfMatchedTiles();

    /**
     * Get the number of attempts.
     *
     * @return Number of attempts.
     */
    @Api
    int getNumberOfAttempts();

    /**
     * Reset the tracked statistics.
     */
    @Api
    void reset();

}
