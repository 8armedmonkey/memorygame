package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface ScoreEntry {

    /**
     * Get the score in the entry.
     *
     * @return Score.
     */
    @Api
    Score getScore();

    /**
     * Get the score data in the entry.
     *
     * @return Score data.
     */
    @Api
    ScoreData getScoreData();

    @Api
    interface Builder {

        /**
         * Set the score in the entry.
         *
         * @param score Score.
         * @return {@link Builder} instance.
         */
        @Api
        Builder score(Score score);

        /**
         * Set the score data in the entry.
         *
         * @param scoreData Score data.
         * @return {@link Builder} instance.
         */
        @Api
        Builder scoreData(ScoreData scoreData);

        /**
         * Create a new {@link ScoreEntry} instance.
         *
         * @return New {@link ScoreEntry} instance.
         */
        @Api
        ScoreEntry build();

    }

}
