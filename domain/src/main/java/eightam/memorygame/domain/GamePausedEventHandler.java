package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;
import sys.event.EventHandler;

@Api
public interface GamePausedEventHandler extends EventHandler<GamePausedEvent> {
}
