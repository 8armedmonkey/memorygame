package eightam.memorygame.domain;

class BoardSizeImpl implements BoardSize {

    private final int count;

    BoardSizeImpl(int count) {
        this.count = count;
    }

    @Override
    public int count() {
        return count;
    }

}
