package eightam.memorygame.domain;

import java.util.Iterator;

class BoardInfoImpl implements BoardInfo {

    private final Board board;

    BoardInfoImpl(Board board) {
        this.board = board;
    }

    @Override
    public BoardSize getBoardSize() {
        return board.getBoardSize();
    }

    @Override
    public boolean isBoardComplete() {
        return board.isBoardComplete();
    }

    @Override
    public Iterator<TileInfo> getTileInfoIterator() {
        return new TileInfoIterator(board);
    }

}
