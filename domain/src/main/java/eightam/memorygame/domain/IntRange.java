package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface IntRange {

    /**
     * Get the start value of the range.
     *
     * @return The range start value.
     */
    @Api
    int startValue();

    /**
     * Get the end value of the range.
     *
     * @return The range end value.
     */
    @Api
    int endValue();

}
