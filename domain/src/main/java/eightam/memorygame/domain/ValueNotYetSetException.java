package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public class ValueNotYetSetException extends RuntimeException {
}
