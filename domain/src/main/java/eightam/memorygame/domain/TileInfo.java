package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface TileInfo {

    @Api
    int getNumericValue();

    @Api
    Tile.Status getStatus();

}
