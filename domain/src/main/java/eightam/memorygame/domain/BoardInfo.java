package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

import java.util.Iterator;

@Api
public interface BoardInfo {

    @Api
    BoardSize getBoardSize();

    @Api
    boolean isBoardComplete();

    @Api
    Iterator<TileInfo> getTileInfoIterator();

}
