package eightam.memorygame.domain;

import java.util.concurrent.atomic.AtomicInteger;

class StatsTrackerImpl implements StatsTracker {

    private AtomicInteger numberOfAttempts;
    private AtomicInteger numberOfMatchedTiles;

    StatsTrackerImpl() {
        numberOfAttempts = new AtomicInteger();
        numberOfMatchedTiles = new AtomicInteger();
    }

    @Override
    public void incrementNumberOfAttempts() {
        numberOfAttempts.addAndGet(1);
    }

    @Override
    public void incrementNumberOfMatchedTiles() {
        numberOfMatchedTiles.addAndGet(2);
    }

    @Override
    public int getNumberOfMatchedTiles() {
        return numberOfMatchedTiles.get();
    }

    @Override
    public int getNumberOfAttempts() {
        return numberOfAttempts.get();
    }

    @Override
    public void reset() {
        numberOfAttempts.set(0);
        numberOfMatchedTiles.set(0);
    }

}
