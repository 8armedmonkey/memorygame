package eightam.memorygame.domain;

import sys.event.Event;
import sys.event.EventBroadcaster;
import sys.event.EventHandler;

import static eightam.memorygame.domain.StatsUtils.buildStats;

class GameImpl implements Game, CountdownTimer.Listener {

    private static final GameStartedEvent GAME_STARTED_EVENT = new GameStartedEvent();
    private static final GamePausedEvent GAME_PAUSED_EVENT = new GamePausedEvent();
    private static final GameRestartedEvent GAME_RESTARTED_EVENT = new GameRestartedEvent();

    private volatile State state;
    private Board board;
    private CountdownTimer timer;
    private StatsTracker statsTracker;
    private EventBroadcaster eventBroadcaster;

    GameImpl(Board board, CountdownTimer timer, StatsTracker statsTracker, EventBroadcaster eventBroadcaster) {
        this.state = State.INIT;
        this.board = board;
        this.timer = timer;
        this.statsTracker = statsTracker;
        this.eventBroadcaster = eventBroadcaster;

        this.timer.setListener(this);
    }

    @Override
    public synchronized void start() throws InvalidGameStateException {
        if (getState() == State.INIT || getState() == State.PAUSED) {
            setState(State.STARTED);

            eventBroadcaster.broadcast(GAME_STARTED_EVENT);
            timer.start();

        } else if (getState() != State.STARTED) {
            throw new InvalidGameStateException();
        }
    }

    @Override
    public synchronized void pause() throws InvalidGameStateException {
        if (getState() == State.STARTED) {
            setState(State.PAUSED);

            eventBroadcaster.broadcast(GAME_PAUSED_EVENT);
            timer.pause();

        } else if (getState() != State.PAUSED) {
            throw new InvalidGameStateException();
        }
    }

    @Override
    public synchronized void restart() {
        board.init(board.getBoardSize());
        timer.reset();
        statsTracker.reset();

        setState(State.STARTED);

        eventBroadcaster.broadcast(GAME_RESTARTED_EVENT);
        timer.start();
    }

    @Override
    public synchronized void open(int tileIndex)
            throws InvalidGameStateException, InvalidTileException, InvalidOpenException {

        assertGameIsStarted(state);
        openBoardTileAndHandleResult(tileIndex);
    }

    @Override
    public synchronized void closeOpenedTiles() {
        board.closeOpenedTiles();
    }

    @Override
    public GameSpecs getGameSpecs() {
        return new GameSpecsImpl(board.getBoardSize(), timer.getDurationMillis());
    }

    @Override
    public State getState() {
        return state;
    }

    @Override
    public boolean isFinished() {
        return state == State.COMPLETED || state == State.TIMER_EXPIRED;
    }

    @Override
    public <E extends Event> EventBroadcaster.Subscription subscribe(
            Class<E> eventClass, EventHandler<E> eventHandler) {

        return eventBroadcaster.subscribe(eventClass, eventHandler);
    }

    @Override
    public void onTimerTick() {
        eventBroadcaster.broadcast(new GameTimerTickEvent(
                timer.getElapsedMillis(), timer.getDurationMillis()));
    }

    @Override
    public void onTimerExpired() {
        setState(State.TIMER_EXPIRED);
        eventBroadcaster.broadcast(new GameTimerExpiredEvent(buildStats(board, timer, statsTracker)));
    }

    @Override
    public void onTimerReset() {
    }

    private void openBoardTileAndHandleResult(int tileIndex) {
        Board.OpenResult result = board.open(tileIndex);

        switch (result) {
            case NO_MATCH:
                onOpenNoMatch();
                break;

            case MATCH:
                onOpenMatch();
                break;

            case COMPLETE:
                onBoardComplete();
                break;
        }
    }

    private void onOpenNoMatch() {
        statsTracker.incrementNumberOfAttempts();
        eventBroadcaster.broadcast(new GameOpenNoMatchEvent());
    }

    private void onOpenMatch() {
        statsTracker.incrementNumberOfAttempts();
        statsTracker.incrementNumberOfMatchedTiles();
        eventBroadcaster.broadcast(new GameOpenMatchEvent());
    }

    private void onBoardComplete() {
        setState(State.COMPLETED);

        timer.pause();

        statsTracker.incrementNumberOfAttempts();
        statsTracker.incrementNumberOfMatchedTiles();

        eventBroadcaster.broadcast(new GameCompletedEvent(buildStats(board, timer, statsTracker)));
    }

    private synchronized void setState(State state) {
        this.state = state;
    }

    private static void assertGameIsStarted(State state) throws InvalidGameStateException {
        if (state != State.STARTED) {
            throw new InvalidGameStateException();
        }
    }

}
