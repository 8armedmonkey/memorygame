package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface KeyValuePair<Key, Value> {

    /**
     * Get the key of the pair.
     *
     * @return Key.
     */
    @Api
    Key getKey();

    /**
     * Get the value of the pair.
     *
     * @return Value.
     */
    @Api
    Value getValue();

}
