package eightam.memorygame.domain;

class ScoringServiceImpl implements ScoringService {

    private static final double COMPLETE_WEIGHT = 0.50;
    private static final double ATTEMPTS_WEIGHT = 0.25;
    private static final double DURATION_WEIGHT = 0.25;

    @Override
    public Score calculateCompleteness(double completenessPercentage) {
        return new ScoreImpl(COMPLETE_WEIGHT * completenessPercentage);
    }

    @Override
    public Score calculate(Stats stats) {
        return new ScoreImpl(
                COMPLETE_WEIGHT * stats.getCompletenessPercentage()
                        + ATTEMPTS_WEIGHT * stats.getAttemptsPercentage()
                        + DURATION_WEIGHT * stats.getDurationPercentage()
        );
    }

}
