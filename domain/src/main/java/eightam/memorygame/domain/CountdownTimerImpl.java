package eightam.memorygame.domain;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

class CountdownTimerImpl implements CountdownTimer {

    static final long DEFAULT_TICK_INTERVAL_MILLIS = 100;

    private final long tickIntervalMillis;
    private final long durationMillis;
    private AtomicLong elapsedTimeMillis;
    private AtomicBoolean started;
    private Listener listener;

    private ScheduledExecutorService executorService;

    CountdownTimerImpl(long durationMillis) {
        this(DEFAULT_TICK_INTERVAL_MILLIS, durationMillis);
    }

    CountdownTimerImpl(
            long tickIntervalMillis,
            long durationMillis) {

        this.tickIntervalMillis = tickIntervalMillis;
        this.durationMillis = durationMillis;
        this.elapsedTimeMillis = new AtomicLong(0);
        this.started = new AtomicBoolean(false);
    }

    @Override
    public synchronized void start() {
        if (!isExpired() && !started.getAndSet(true)) {
            executorService = Executors.newSingleThreadScheduledExecutor();
            executorService.scheduleAtFixedRate(
                    this::updateElapsedTimeMillis,
                    tickIntervalMillis,
                    tickIntervalMillis,
                    TimeUnit.MILLISECONDS);

            notifyOnTimerTick();
        }
    }

    @Override
    public synchronized void pause() {
        if (started.getAndSet(false) && executorService != null) {
            executorService.shutdownNow();
            executorService = null;
        }
    }

    @Override
    public synchronized void reset() {
        pause();
        elapsedTimeMillis.set(0);

        notifyOnTimerReset();
    }

    @Override
    public long getElapsedMillis() {
        return elapsedTimeMillis.get();
    }

    @Override
    public long getDurationMillis() {
        return durationMillis;
    }

    @Override
    public boolean isRunning() {
        return started.get();
    }

    @Override
    public boolean isExpired() {
        return elapsedTimeMillis.get() >= durationMillis;
    }

    @Override
    public void setListener(Listener listener) {
        this.listener = listener;
    }

    private void updateElapsedTimeMillis() {
        if (elapsedTimeMillis.addAndGet(tickIntervalMillis) >= durationMillis) {
            pause();
            notifyOnTimerExpired();
        } else {
            notifyOnTimerTick();
        }
    }

    private void notifyOnTimerExpired() {
        if (listener != null) {
            listener.onTimerExpired();
        }
    }

    private void notifyOnTimerTick() {
        if (listener != null) {
            listener.onTimerTick();
        }
    }

    private void notifyOnTimerReset() {
        if (listener != null) {
            listener.onTimerReset();
        }
    }

}
