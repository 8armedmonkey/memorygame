package eightam.memorygame.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

class IntPairImpl implements IntPair {

    private List<Integer> pair;

    IntPairImpl() {
        pair = Collections.synchronizedList(new ArrayList<>(2));
    }

    @Override
    public int getFirst() throws ValueNotYetSetException {
        if (isFirstSet()) {
            return pair.get(0);
        } else {
            throw new ValueNotYetSetException();
        }
    }

    @Override
    public int getSecond() throws ValueNotYetSetException {
        if (isSecondSet()) {
            return pair.get(1);
        } else {
            throw new ValueNotYetSetException();
        }
    }

    @Override
    public void put(int number) throws MaxCapacityReachedException {
        if (!isSecondSet()) {
            pair.add(number);
        } else {
            throw new MaxCapacityReachedException();
        }
    }

    @Override
    public void removeFirst() throws ValueNotYetSetException {
        if (isFirstSet()) {
            pair.remove(0);
        } else {
            throw new ValueNotYetSetException();
        }
    }

    @Override
    public void removeSecond() throws ValueNotYetSetException {
        if (isSecondSet()) {
            pair.remove(1);
        } else {
            throw new ValueNotYetSetException();
        }
    }

    @Override
    public void clear() {
        pair.clear();
    }

    @Override
    public boolean isFirstSet() {
        return pair.size() >= 1;
    }

    @Override
    public boolean isSecondSet() {
        return pair.size() == 2;
    }

    @Override
    public boolean isEmpty() {
        return pair.isEmpty();
    }

    @Override
    public Iterator<Integer> iterator() {
        return pair.iterator();
    }

}
