package eightam.memorygame.domain;

import static eightam.memorygame.domain.Factories.newStatsBuilder;
import static java.lang.Math.min;

final class StatsUtils {

    private StatsUtils() {
    }

    static Stats buildStats(Board board, CountdownTimer timer, StatsTracker statsTracker) {
        return newStatsBuilder()
                .numberOfTiles(board.getBoardSize().count())
                .numberOfMatchedTiles(statsTracker.getNumberOfMatchedTiles())
                .numberOfAttempts(statsTracker.getNumberOfAttempts())
                .durationMillis(timer.getDurationMillis())
                .durationToCompleteMillis(min(timer.getDurationMillis(), timer.getElapsedMillis()))
                .build();
    }

}
