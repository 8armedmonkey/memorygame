package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;
import sys.event.EventBroadcaster;

@Api
public final class Factories {

    private Factories() {
    }

    @Api
    public static Board newBoard() {
        return new BoardImpl();
    }

    @Api
    public static BoardSize newBoardSize(int count) {
        return new BoardSizeImpl(count);
    }

    @Api
    public static ScoringService newScoringService() {
        return new ScoringServiceImpl();
    }

    @Api
    public static CountdownTimer newCountdownTimer(long durationMillis) {
        return new CountdownTimerImpl(durationMillis);
    }

    @Api
    public static CountdownTimer newCountdownTimer(long tickIntervalMillis, long durationMillis) {
        return new CountdownTimerImpl(tickIntervalMillis, durationMillis);
    }

    @Api
    public static Stats.Builder newStatsBuilder() {
        return new StatsImpl.Builder();
    }

    @Api
    public static ScoreData.Builder newScoreDataBuilder() {
        return new ScoreDataImpl.Builder();
    }

    @Api
    public static ScoreEntry.Builder newScoreEntryBuilder() {
        return new ScoreEntryImpl.Builder();
    }

    @Api
    public static GameSpecs.Builder newGameSpecsBuilder() {
        return new GameSpecsImpl.Builder();
    }

    @Api
    public static StatsTracker newStatsTracker() {
        return new StatsTrackerImpl();
    }

    @Api
    public static Game newGame(GameSpecs specs, StatsTracker statsTracker, EventBroadcaster eventBroadcaster) {
        Board board = newBoard();
        board.init(specs.getBoardSize());

        CountdownTimer timer = newCountdownTimer(specs.getDurationMillis());

        return newGame(board, timer, statsTracker, eventBroadcaster);
    }

    @Api
    public static Game newGame(Board board, CountdownTimer timer, StatsTracker statsTracker, EventBroadcaster eventBroadcaster) {
        return new GameImpl(board, timer, statsTracker, eventBroadcaster);
    }

}
