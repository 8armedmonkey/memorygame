package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public class BoardNotInitializedException extends RuntimeException {
}
