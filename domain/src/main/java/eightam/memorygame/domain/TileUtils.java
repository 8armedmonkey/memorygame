package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public final class TileUtils {

    private TileUtils() {
    }

    @Api
    public static void setTilesStatuses(Tile.Status status, Tile... tiles) {
        for (Tile tile : tiles) {
            tile.setStatus(status);
        }
    }

    @Api
    public static boolean expectTilesStatuses(Tile.Status status, Tile... tiles) {
        for (Tile tile : tiles) {
            if (tile.getStatus() != status) {
                return false;
            }
        }
        return true;
    }

}
