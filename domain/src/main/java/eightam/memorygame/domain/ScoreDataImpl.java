package eightam.memorygame.domain;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

class ScoreDataImpl implements ScoreData {

    private final Map<String, StringKeyValuePair> data;

    ScoreDataImpl() {
        data = new HashMap<>();
    }

    @Override
    public void put(StringKeyValuePair keyValuePair) {
        data.put(keyValuePair.getKey(), keyValuePair);
    }

    @Override
    public void remove(String key) {
        data.remove(key);
    }

    @Override
    public boolean contains(String key) {
        return data.containsKey(key);
    }

    @Override
    public int size() {
        return data.size();
    }

    @Override
    public Iterator<StringKeyValuePair> iterator() {
        return data.values().iterator();
    }

    static class Builder implements ScoreData.Builder {

        private Map<String, StringKeyValuePair> data;

        Builder() {
            data = new HashMap<>();
        }

        @Override
        public ScoreData.Builder put(StringKeyValuePair keyValuePair) {
            data.put(keyValuePair.getKey(), keyValuePair);
            return this;
        }

        @Override
        public ScoreData build() {
            ScoreData scoreData = new ScoreDataImpl();

            for (StringKeyValuePair pair : data.values()) {
                scoreData.put(pair);
            }
            return scoreData;
        }

    }

}
