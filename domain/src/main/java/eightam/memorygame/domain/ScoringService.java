package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface ScoringService {

    /**
     * Calculate the score based on completeness only.
     * The completeness score could be used as intermediate score in the game.
     *
     * @param completenessPercentage The completeness percentage.
     * @return Completeness score.
     */
    @Api
    Score calculateCompleteness(double completenessPercentage);

    /**
     * Calculate the score based on certain statistics.
     *
     * @param stats The statistics by which the score is calculated.
     * @return Score.
     */
    @Api
    Score calculate(Stats stats);

}
