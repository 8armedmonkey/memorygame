package eightam.memorygame.domain;

import java.util.Iterator;

class TileInfoIterator implements Iterator<TileInfo> {

    private final Iterator<Tile> tileIterator;

    TileInfoIterator(Iterable<Tile> tiles) {
        tileIterator = tiles.iterator();
    }

    @Override
    public boolean hasNext() {
        return tileIterator.hasNext();
    }

    @Override
    public TileInfo next() {
        return new TileInfoImpl(tileIterator.next());
    }

}
