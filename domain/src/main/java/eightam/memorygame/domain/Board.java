package eightam.memorygame.domain;

import eightam.memorygame.annotation.Api;

@Api
public interface Board extends Iterable<Tile> {

    /**
     * Initialize the board with tiles.
     *
     * @param size The board size.
     * @throws InvalidBoardSizeException If the number of tiles is not an even number greater than zero.
     * @throws InvalidBoardSizeException If the width or the height of the board are not greater than zero.
     */
    @Api
    void init(BoardSize size) throws InvalidBoardSizeException;

    /**
     * Get the size of the board.
     *
     * @return The board size.
     * @throws BoardNotInitializedException If the board has not been initialized with a valid size.
     */
    @Api
    BoardSize getBoardSize() throws BoardNotInitializedException;

    /**
     * Open a tile specified by the index.
     *
     * @param tileIndex The index of the tile within the board.
     * @return Result of the open operation.
     * @throws InvalidTileException If the tile does not exist, is already opened or matched.
     * @throws InvalidOpenException If an attempt to open more than two tiles at once is made.
     */
    @Api
    OpenResult open(int tileIndex) throws InvalidTileException, InvalidOpenException;

    /**
     * Close opened tiles that do not match.
     * Opened tiles that do not match have to be closed first before another open is made.
     */
    @Api
    void closeOpenedTiles();

    /**
     * Check if the board is complete (i.e: all tiles are matched).
     *
     * @return True if it is complete, false otherwise.
     */
    @Api
    boolean isBoardComplete();

    /**
     * The result of an open attempt.
     * <ul>
     * <li>PENDING: one tile is opened, waiting for another one to be opened.</li>
     * <li>MATCH: two tiles are opened and they match.</li>
     * <li>NO_MATCH: two tiles are opened and they do not match.</li>
     * <li>COMPLETE: all tiles have been opened and matched.</li>
     * </ul>
     */
    @Api
    enum OpenResult {

        PENDING, MATCH, NO_MATCH, COMPLETE

    }

}
