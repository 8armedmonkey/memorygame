package eightam.memorygame.util;

import eightam.memorygame.grid.RowColumn;
import org.junit.Test;

import static eightam.memorygame.grid.GridUtils.arrayIndexToRowColumn;
import static org.junit.Assert.assertEquals;

public class TestGridUtils {

    @Test
    public void arrayIndexToRowColumn_arrayIndexTransformedToRowColumnIndices_1() {
        int numOfColumns = 2;

        RowColumn rc00 = arrayIndexToRowColumn(0, numOfColumns);
        RowColumn rc01 = arrayIndexToRowColumn(1, numOfColumns);
        RowColumn rc10 = arrayIndexToRowColumn(2, numOfColumns);
        RowColumn rc11 = arrayIndexToRowColumn(3, numOfColumns);

        assertEquals(0, rc00.getRow());
        assertEquals(0, rc00.getColumn());

        assertEquals(0, rc01.getRow());
        assertEquals(1, rc01.getColumn());

        assertEquals(1, rc10.getRow());
        assertEquals(0, rc10.getColumn());

        assertEquals(1, rc11.getRow());
        assertEquals(1, rc11.getColumn());
    }

    @Test
    public void arrayIndexToRowColumn_arrayIndexTransformedToRowColumnIndices_2() {
        int numOfColumns = 2;

        RowColumn rc00 = arrayIndexToRowColumn(0, numOfColumns);
        RowColumn rc01 = arrayIndexToRowColumn(1, numOfColumns);
        RowColumn rc10 = arrayIndexToRowColumn(2, numOfColumns);
        RowColumn rc11 = arrayIndexToRowColumn(3, numOfColumns);
        RowColumn rc20 = arrayIndexToRowColumn(4, numOfColumns);
        RowColumn rc21 = arrayIndexToRowColumn(5, numOfColumns);

        assertEquals(0, rc00.getRow());
        assertEquals(0, rc00.getColumn());

        assertEquals(0, rc01.getRow());
        assertEquals(1, rc01.getColumn());

        assertEquals(1, rc10.getRow());
        assertEquals(0, rc10.getColumn());

        assertEquals(1, rc11.getRow());
        assertEquals(1, rc11.getColumn());

        assertEquals(2, rc20.getRow());
        assertEquals(0, rc20.getColumn());

        assertEquals(2, rc21.getRow());
        assertEquals(1, rc21.getColumn());
    }

}
