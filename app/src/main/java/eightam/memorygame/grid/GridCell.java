package eightam.memorygame.grid;

public class GridCell {

    private char[] content;
    private Alignment alignment;

    public GridCell() {
        this(new char[0], Alignment.LEFT);
    }

    public GridCell(char[] content, Alignment alignment) {
        this.content = content;
        this.alignment = alignment;
    }

    public char[] getContent() {
        return content;
    }

    public void setContent(char[] content) {
        this.content = content;
    }

    public Alignment getAlignment() {
        return alignment;
    }

    public void setAlignment(Alignment alignment) {
        this.alignment = alignment;
    }

    public enum Alignment {

        LEFT, CENTER, RIGHT

    }

}
