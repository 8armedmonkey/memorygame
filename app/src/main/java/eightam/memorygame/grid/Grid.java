package eightam.memorygame.grid;

public class Grid {

    private int numOfRows;
    private int numOfCols;
    private GridCell[][] cells;

    public Grid(int numOfRows, int numOfCols) {
        assertPositive(numOfRows, numOfCols);

        this.numOfRows = numOfRows;
        this.numOfCols = numOfCols;
        this.cells = populateCells(numOfRows, numOfCols);
    }

    public GridCell[] get(int row) {
        return cells[row];
    }

    public GridCell get(int row, int col) {
        return cells[row][col];
    }

    public GridCell get(RowColumn rowCol) {
        return get(rowCol.getRow(), rowCol.getColumn());
    }

    public void set(GridCell cell, int row, int col) {
        assertNotNull(cell);
        cells[row][col] = cell;
    }

    public int getNumOfRows() {
        return numOfRows;
    }

    public int getNumOfCols() {
        return numOfCols;
    }

    private static GridCell[][] populateCells(int numOfRows, int numOfCols) {
        GridCell[][] cells = new GridCell[numOfRows][numOfCols];

        for (int i = 0; i < numOfRows; i++) {
            for (int j = 0; j < numOfCols; j++) {
                cells[i][j] = new GridCell();
            }
        }

        return cells;
    }

    private static void assertPositive(int... numbers) {
        for (int number : numbers) {
            if (number <= 0) throw new IllegalArgumentException();
        }
    }

    private static void assertNotNull(GridCell cell) {
        if (cell == null) throw new IllegalArgumentException();
    }

}
