package eightam.memorygame.grid;

public final class GridUtils {

    private GridUtils() {
    }

    /**
     * Convert a one dimensional array index into a two dimensional grid index (as row index and column index).
     *
     * @param index        The array index.
     * @param numOfColumns The number of columns in the grid.
     * @return Row index and column index.
     */
    public static RowColumn arrayIndexToRowColumn(int index, int numOfColumns) {
        return new RowColumn(index / numOfColumns, index % numOfColumns);
    }

}
