package eightam.memorygame.menu;

public class MenuItem {

    private char[] key;
    private String label;
    private Runnable action;

    public MenuItem(char[] key, String label, Runnable action) {
        this.key = key;
        this.label = label;
        this.action = action;
    }

    public void select() {
        if (action != null) {
            action.run();
        }
    }

    public char[] getKey() {
        return key;
    }

    public void setKey(char[] key) {
        this.key = key;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public Runnable getAction() {
        return action;
    }

    public void setAction(Runnable action) {
        this.action = action;
    }

}
