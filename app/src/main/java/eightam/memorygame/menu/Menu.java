package eightam.memorygame.menu;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Menu implements Iterable<MenuItem> {

    private List<MenuItem> items;

    public Menu() {
        items = new ArrayList<>();
    }

    @Override
    public Iterator<MenuItem> iterator() {
        return items.iterator();
    }

    public void add(MenuItem item) {
        items.add(item);
    }

    public void remove(MenuItem item) {
        items.remove(item);
    }

    public void clear() {
        items.clear();
    }

}
