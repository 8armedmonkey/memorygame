package eightam.memorygame.util;

public final class ThreadUtils {

    private ThreadUtils() {
    }

    public static void start(Thread t) {
        t.start();
    }

    public static void join(Thread t) {
        try {
            t.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
