package eightam.memorygame.util;

import java.io.PrintStream;

public final class PrintUtils {

    private PrintUtils() {
    }

    public static void repeat(PrintStream stream, int count, char... chars) {
        for (int i = 0; i < count; i++) {
            stream.print(chars);
        }
    }

}
