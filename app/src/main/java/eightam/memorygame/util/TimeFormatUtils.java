package eightam.memorygame.util;

import static java.util.concurrent.TimeUnit.*;

public final class TimeFormatUtils {

    private TimeFormatUtils() {
    }

    public static String duration(long millis) {
        long totalSeconds = SECONDS.convert(millis, MILLISECONDS);
        long minutes = MINUTES.convert(totalSeconds, SECONDS);
        long seconds = totalSeconds - SECONDS.convert(minutes, MINUTES);

        return String.format("%02d:%02d", minutes, seconds);
    }

}
