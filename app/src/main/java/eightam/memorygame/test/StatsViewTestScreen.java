package eightam.memorygame.test;

import eightam.memorygame.domain.Score;
import eightam.memorygame.domain.Stats;
import eightam.memorygame.presentation.BaseScreen;
import eightam.memorygame.view.StatsView;

import java.io.PrintStream;

import static eightam.memorygame.domain.Factories.newScoringService;
import static eightam.memorygame.domain.Factories.newStatsBuilder;

public class StatsViewTestScreen extends BaseScreen {

    public StatsViewTestScreen(PrintStream printStream) {
        super(printStream);
    }

    @Override
    public void start() {
        Stats stats = newStatsBuilder()
                .numberOfTiles(8)
                .numberOfMatchedTiles(2)
                .numberOfAttempts(5)
                .durationMillis(5000)
                .durationToCompleteMillis(1000)
                .build();

        Score score = newScoringService().calculate(stats);

        StatsView view = new StatsView(stats, score);
        view.draw(printStream);
    }

}
