package eightam.memorygame.test;

import eightam.memorygame.menu.Menu;
import eightam.memorygame.menu.MenuItem;
import eightam.memorygame.presentation.BaseScreen;
import eightam.memorygame.view.MenuSpecs;
import eightam.memorygame.view.MenuView;

import java.io.PrintStream;

public class MenuViewTestScreen extends BaseScreen {

    public MenuViewTestScreen(PrintStream printStream) {
        super(printStream);
    }

    @Override
    public void start() {
        MenuSpecs specs = new MenuSpecs.Builder()
                .title("Memory Game")
                .description("Lorem ipsum dolor sit amet, consectetur adipiscing elit, "
                        + "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.")
                .wrapLength(60)
                .build();

        Menu menu = new Menu();
        menu.add(new MenuItem(new char[]{'P'}, "[P] Play Game", () -> {}));
        menu.add(new MenuItem(new char[]{'S'}, "[S] Settings", () -> {}));
        menu.add(new MenuItem(new char[]{'X'}, "[X] Exit", () -> {}));

        MenuView view = new MenuView(specs, menu);
        view.draw(printStream);
    }

}
