package eightam.memorygame.test;

import eightam.memorygame.presentation.BaseScreen;
import eightam.memorygame.view.TimerView;

import java.io.PrintStream;

public class TimerViewTestScreen extends BaseScreen {

    private static final long ZERO_SECOND = 0;
    private static final long FIVE_MINUTES = 5 * 60 * 1000;
    private static final long SEVEN_MINUTES_AND_29_SECONDS = (7 * 60 * 1000) + (29 * 1000);
    private static final long ONE_HOUR = 60 * 60 * 1000;
    private static final long TWO_HOURS_41_MINUTES_59_SECONDS = (2 * 60 * 60 * 1000) + (41 * 60 * 1000) + (59 * 1000);

    public TimerViewTestScreen(PrintStream printStream) {
        super(printStream);
    }

    @Override
    public void start() {
        TimerView timerView = new TimerView();

        printStream.print("0 sec: ");
        timerView.setTimeMillis(ZERO_SECOND);
        timerView.draw(printStream);
        printStream.println();

        printStream.print("5 min: ");
        timerView.setTimeMillis(FIVE_MINUTES);
        timerView.draw(printStream);
        printStream.println();

        printStream.print("7 min 29 secs: ");
        timerView.setTimeMillis(SEVEN_MINUTES_AND_29_SECONDS);
        timerView.draw(printStream);
        printStream.println();

        printStream.print("1 hour: ");
        timerView.setTimeMillis(ONE_HOUR);
        timerView.draw(printStream);
        printStream.println();

        printStream.print("2 hours 41 min 59 secs: ");
        timerView.setTimeMillis(TWO_HOURS_41_MINUTES_59_SECONDS);
        timerView.draw(printStream);
        printStream.println();
    }

}
