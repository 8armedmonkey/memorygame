package eightam.memorygame.test;

import eightam.memorygame.presentation.BaseScreen;
import eightam.memorygame.grid.Grid;
import eightam.memorygame.grid.GridCell;
import eightam.memorygame.view.GridSpecs;
import eightam.memorygame.view.GridView;

import java.io.PrintStream;

import static eightam.memorygame.grid.GridCell.Alignment.*;

public class GridViewTestScreen extends BaseScreen {

    private static final String[] TEXTS = {"XXX", "XX", "GAME", "HELLO WORLD", ""};
    private static final int NUM_OF_ROWS = 3;
    private static final int NUM_OF_COLS = TEXTS.length;
    private static final int COL_WIDTH_IN_CHARS = 7;

    public GridViewTestScreen(PrintStream printStream) {
        super(printStream);
    }

    @Override
    public void start() {
        GridSpecs specs = new GridSpecs.Builder()
                .numOfRows(NUM_OF_ROWS)
                .numOfColumns(NUM_OF_COLS)
                .columnWidthInChars(COL_WIDTH_IN_CHARS)
                .build();

        Grid grid = new Grid(NUM_OF_ROWS, NUM_OF_COLS);

        for (int i = 0; i < TEXTS.length; i++) {
            grid.set(new GridCell(TEXTS[i].toCharArray(), LEFT), 0, i);
            grid.set(new GridCell(TEXTS[i].toCharArray(), CENTER), 1, i);
            grid.set(new GridCell(TEXTS[i].toCharArray(), RIGHT), 2, i);
        }

        GridView view = new GridView(specs, grid);
        view.draw(printStream);
    }

}
