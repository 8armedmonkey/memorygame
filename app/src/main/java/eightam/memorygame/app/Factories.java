package eightam.memorygame.app;

import sys.event.EventBroadcaster;
import sys.event.EventBroadcasterFactoryImpl;

public final class Factories {

    public static EventBroadcaster newEventBroadcaster() {
        return new EventBroadcasterFactoryImpl().createEventBroadcaster();
    }

}
