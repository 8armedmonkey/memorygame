package eightam.memorygame.app;

import eightam.memorygame.presentation.Screen;
import eightam.memorygame.test.StatsViewTestScreen;

public class Main {

    public static void main(String[] args) {
        Screen screen = new StatsViewTestScreen(System.out);
        screen.start();
    }

}
