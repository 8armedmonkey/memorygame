package eightam.memorygame.presentation;

import java.io.PrintStream;

public abstract class BaseScreen implements Screen {

    protected final PrintStream printStream;

    public BaseScreen(PrintStream printStream) {
        this.printStream = printStream;
    }

}
