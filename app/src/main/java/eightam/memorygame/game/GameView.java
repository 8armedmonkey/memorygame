package eightam.memorygame.game;

import eightam.memorygame.domain.Board;
import eightam.memorygame.domain.Score;
import eightam.memorygame.domain.Stats;

public interface GameView {

    void showBoard(Board board);

    void showGameCompletedMessage(Stats stats, Score score);

    void showGameTimerExpiredMessage(Stats stats, Score score);

}
