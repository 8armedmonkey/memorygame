package eightam.memorygame.game;

import eightam.memorygame.domain.*;
import eightam.memorygame.presentation.BaseScreen;
import eightam.memorygame.view.GridView;
import eightam.memorygame.view.StatsView;
import eightam.memorygame.view.TimerView;

import java.io.InputStream;
import java.io.PrintStream;
import java.util.Scanner;

import static eightam.memorygame.app.Factories.newEventBroadcaster;
import static eightam.memorygame.domain.Factories.*;

public class GameScreen extends BaseScreen {

    private Scanner scanner;
    private Game game;

    private GridView gridView;
    private TimerView timerView;
    private StatsView statsView;

    public GameScreen(PrintStream printStream, InputStream inputStream) {
        super(printStream);
        scanner = new Scanner(inputStream);
    }

    @Override
    public void start() {
        init();
        loop();
    }

    private void init() {
        game = newGame(
                newGameSpecsBuilder()
                        .boardSize(newBoardSize(4))
                        .durationMillis(60000)
                        .build(),
                newStatsTracker(),
                newEventBroadcaster()
        );

        game.subscribe(GameOpenNoMatchEvent.class, e -> onGameOpenNoMatch());
        game.subscribe(GameOpenMatchEvent.class, e -> onGameOpenMatch());
        game.subscribe(GameCompletedEvent.class, this::onGameCompleted);
        game.subscribe(GameTimerExpiredEvent.class, this::onGameTimerExpired);
    }

    private void loop() {
        while (!game.isFinished()) {
        }
    }

    private void onGameOpenNoMatch() {
        game.closeOpenedTiles();
    }

    private void onGameOpenMatch() {
    }

    private void onGameCompleted(GameCompletedEvent event) {
        onStatsAvailable(event.getStats());
    }

    private void onGameTimerExpired(GameTimerExpiredEvent event) {
        onStatsAvailable(event.getStats());
    }

    private void onStatsAvailable(Stats stats) {
        statsView = new StatsView(stats, newScoringService().calculate(stats));
        statsView.draw(printStream);
    }

}
