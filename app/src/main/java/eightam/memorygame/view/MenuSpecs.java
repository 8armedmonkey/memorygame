package eightam.memorygame.view;

public class MenuSpecs {

    private String title;
    private String description;
    private int wrapLength;

    private MenuSpecs(String title, String description, int wrapLength) {
        this.title = title;
        this.description = description;
        this.wrapLength = wrapLength;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public int getWrapLength() {
        return wrapLength;
    }

    public static class Builder {

        private String title;
        private String description;
        private int wrapLength;

        public Builder title(String title) {
            this.title = title;
            return this;
        }

        public Builder description(String description) {
            this.description = description;
            return this;
        }

        public Builder wrapLength(int wrapLength) {
            this.wrapLength = wrapLength;
            return this;
        }

        public MenuSpecs build() {
            return new MenuSpecs(title, description, wrapLength);
        }

    }

}
