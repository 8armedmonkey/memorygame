package eightam.memorygame.view;

import eightam.memorygame.util.TimeFormatUtils;

import java.io.PrintStream;

public class TimerView implements ConsoleView {

    private long timeMillis;

    @Override
    public void draw(PrintStream stream) {
        stream.println(TimeFormatUtils.duration(timeMillis));
    }

    public void setTimeMillis(long timeMillis) {
        this.timeMillis = timeMillis;
    }

}
