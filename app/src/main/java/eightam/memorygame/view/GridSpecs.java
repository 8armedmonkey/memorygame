package eightam.memorygame.view;

public class GridSpecs {

    private final int numOfRows;
    private final int numOfColumns;
    private final int columnWidthInChars;

    private GridSpecs(
            int numOfRows,
            int numOfColumns,
            int columnWidthInChars) {

        assertPositive(numOfRows, numOfColumns, columnWidthInChars);

        this.numOfRows = numOfRows;
        this.numOfColumns = numOfColumns;
        this.columnWidthInChars = columnWidthInChars;
    }

    public int getNumOfRows() {
        return numOfRows;
    }

    public int getNumOfColumns() {
        return numOfColumns;
    }

    public int getColumnWidthInChars() {
        return columnWidthInChars;
    }

    public static class Builder {

        private int numOfRows;
        private int numOfColumns;
        private int columnWidthInChars;

        public Builder numOfRows(int numOfRows) {
            assertPositive(numOfRows);
            this.numOfRows = numOfRows;

            return this;
        }

        public Builder numOfColumns(int numOfColumns) {
            assertPositive(numOfColumns);
            this.numOfColumns = numOfColumns;

            return this;
        }

        public Builder columnWidthInChars(int columnWidthInChars) {
            assertPositive(columnWidthInChars);
            this.columnWidthInChars = columnWidthInChars;

            return this;
        }

        public GridSpecs build() {
            return new GridSpecs(
                    numOfRows,
                    numOfColumns,
                    columnWidthInChars);
        }

    }

    private static void assertPositive(int... numbers) {
        for (int number : numbers) {
            if (number <= 0) throw new IllegalArgumentException();
        }
    }

}
