package eightam.memorygame.view;

import java.io.PrintStream;

public interface ConsoleView {

    void draw(PrintStream stream);

}
