package eightam.memorygame.view;

import eightam.memorygame.domain.Score;
import eightam.memorygame.domain.Stats;

import java.io.PrintStream;

import static eightam.memorygame.util.TimeFormatUtils.duration;
import static java.lang.String.format;
import static java.util.Locale.ENGLISH;

public class StatsView implements ConsoleView {

    private Stats stats;
    private Score score;

    public StatsView(Stats stats, Score score) {
        this.stats = stats;
        this.score = score;
    }

    @Override
    public void draw(PrintStream stream) {
        stream.println("STATISTICS");
        stream.println("----------");
        stream.println(format(ENGLISH, "Completeness: %4s%%",
                Double.valueOf(stats.getCompletenessPercentage() * 100).intValue()));
        stream.println(format(ENGLISH, "Attempts    : %5s",
                stats.getNumberOfAttempts()));
        stream.println(format(ENGLISH, "Time        : %5s",
                duration(stats.getDurationToCompleteMillis())));
        stream.println(format(ENGLISH, "Score       : %5s",
                Double.valueOf(score.getValue() * 100).intValue()));
        stream.println();
    }

}
