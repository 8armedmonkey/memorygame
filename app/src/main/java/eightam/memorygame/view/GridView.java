package eightam.memorygame.view;

import eightam.memorygame.grid.Grid;
import eightam.memorygame.grid.GridCell;

import java.io.PrintStream;
import java.util.Arrays;

import static eightam.memorygame.util.PrintUtils.repeat;

public class GridView implements ConsoleView {

    private final GridSpecs specs;
    private final Grid grid;

    public GridView(GridSpecs specs, Grid grid) {
        this.specs = specs;

        if (grid != null) {
            this.grid = grid;
        } else {
            this.grid = new Grid(specs.getNumOfRows(), specs.getNumOfColumns());
        }
    }

    @Override
    public void draw(PrintStream stream) {
        final int numOfRows = specs.getNumOfRows();
        final int numOfCols = specs.getNumOfColumns();
        final int colWidth = specs.getColumnWidthInChars();

        for (int i = 0; i < numOfRows; i++) {
            if (i == 0) {
                drawHorizontalBorder(stream, numOfCols, colWidth);
                endLine(stream);
            }

            drawRowSpaces(stream, numOfCols, colWidth);
            endLine(stream);

            drawGridCells(stream, grid.get(i), numOfCols, colWidth);
            endLine(stream);

            drawRowSpaces(stream, numOfCols, colWidth);
            endLine(stream);

            drawHorizontalBorder(stream, numOfCols, colWidth);
            endLine(stream);
        }
    }

    private static void drawHorizontalBorder(PrintStream stream, int numOfColumns, int columnWidthInChars) {
        for (int i = 0; i < numOfColumns; i++) {
            stream.print('+');

            repeat(stream, columnWidthInChars, '-');
        }
        stream.print('+');
    }

    private static void drawVerticalBorder(PrintStream stream) {
        stream.print('|');
    }

    private static void drawRowSpaces(PrintStream stream, int numOfColumns, int columnWidthInChars) {
        for (int j = 0; j < numOfColumns; j++) {
            drawVerticalBorder(stream);
            drawGridCellSpaces(stream, columnWidthInChars);
        }

        drawVerticalBorder(stream);
    }

    private static void drawGridCells(PrintStream stream, GridCell[] cells, int numOfColumns, int columnWidthInChars) {
        for (int j = 0; j < numOfColumns; j++) {
            drawVerticalBorder(stream);
            drawGridCell(stream, cells[j], columnWidthInChars);
        }

        drawVerticalBorder(stream);
    }

    private static void drawGridCellSpaces(PrintStream stream, int columnWidthInChars) {
        repeat(stream, columnWidthInChars, ' ');
    }

    private static void drawGridCell(PrintStream stream, GridCell cell, int columnWidthInChars) {
        char[] content = new char[columnWidthInChars];

        int contentWidthInChars = Math.min(cell.getContent().length, columnWidthInChars);
        int start = getCellContentStartPosition(cell.getAlignment(), columnWidthInChars, contentWidthInChars);

        Arrays.fill(content, 0, start, ' ');
        System.arraycopy(cell.getContent(), 0, content, start, contentWidthInChars);
        Arrays.fill(content, start + contentWidthInChars, content.length, ' ');

        stream.print(content);
    }

    private static int getCellContentStartPosition(
            GridCell.Alignment alignment, int columnWidthInChars, int contentWidthInChars) {

        switch (alignment) {
            case LEFT:
                return 0;

            case CENTER:
                return Math.max(0, (columnWidthInChars - contentWidthInChars) / 2);

            case RIGHT:
                return Math.max(0, columnWidthInChars - contentWidthInChars);
        }
        return 0;
    }

    private static void endLine(PrintStream stream) {
        stream.println();
    }

}
