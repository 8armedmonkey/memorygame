package eightam.memorygame.view;

import eightam.memorygame.menu.Menu;
import eightam.memorygame.menu.MenuItem;
import org.apache.commons.lang.WordUtils;

import java.io.PrintStream;

import static eightam.memorygame.util.PrintUtils.repeat;
import static java.lang.String.format;

public class MenuView implements ConsoleView {

    private MenuSpecs specs;
    private Menu menu;

    public MenuView(MenuSpecs specs, Menu menu) {
        this.specs = specs;
        this.menu = menu;
    }

    @Override
    public void draw(PrintStream stream) {
        drawTitle(stream, specs.getTitle());
        drawDescription(stream, specs.getDescription(), specs.getWrapLength());
        drawMenu(stream, menu);
    }

    private static void drawTitle(PrintStream stream, String title) {
        stream.println();
        stream.println(title.toUpperCase());
        repeat(stream, title.length(), '-');
        stream.println();
    }

    private static void drawDescription(PrintStream stream, String description, int wrapLength) {
        stream.println(WordUtils.wrap(description, wrapLength));
        stream.println();
    }

    private static void drawMenu(PrintStream stream, Menu menu) {
        for (MenuItem item : menu) {
            stream.println(format("\t%s", item.getLabel()));
        }
        stream.println();
    }

}
